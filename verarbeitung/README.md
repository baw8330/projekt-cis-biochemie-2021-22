# Projekt CiS-Projekt 2021/22

Processing-Package to generate a theoretical graph for citations and references of given input publications.

## Usage/Examples

```python
from verarbeitung.process_main import Processing


def main(url_list):
    Processing(url_list)
```

Grundlegender Prozess:
Es wird von der UI eine Liste an DOIs an die Verarbeitung übergeben und diese wird dann umgewandelt in eine Knoten-und
Kantenmenge, welche die Zitierungen darstellen. Die Informationen über die Paper und die Zitierungen kommen von der
Input Gruppe über den Aufruf von der Funktion Publication. Die Knoten- und Kantenmengen werden in Form einer Json Datei
an den Output übergeben.

## Files and functions in directory

get_pub_from_input.py:

```python
def get_pub(pub_doi, test_var)
```

- Gibt für eine DOI ein Klassenobjekt zurück, in dem alle nötigen Informationen gespeichert sind.

process_main.py:

```python
def Processing(url_list)
```

- Überprüft, ob bereits eine Json Datei existiert und ruft dann entweder die Funktion auf, um einen neuen Graphen zu
  erstellen oder die Funktion, um einen Vorhandenen zu updaten.

start.script.py:

- Wird benötigt, um die Dateien ordnerübergreifend aufzurufen. Nur fürs interne Testen der 	
  Funktionalität

<name>.json:

- sind momentan Beispiele, die an den Output übergeben werden könnten.

## Testing

python -m unittest discover verarbeitung/test -v

## Authors

- Donna Löding
- Alina Molkentin
- Judith Große
- Malte Schokolowski