"""
    This file is for testing purposes only. We left it in the directory for ease of use.
    To use it you need to shift it into the main directory of the project
"""

import sys
import gc
from pathlib import Path
from verarbeitung.process_main import Processing



doi_list = []
doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
#doi_list.append('https://doi.org/10.1021/acs.jcim.9b00249')
#doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203')
#doi_list.append('https://doi.org/10.1021/acs.jmedchem.0c01332')
#doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.6b00709')
#doi_list.append('https://doi.org/10.1021/acs.chemrev.8b00728')
#doi_list.append('https://pubs.acs.org/doi/10.1021/acs.chemrestox.0c00006')#
#doi_list.append('https://doi.org/10.1021/acs.chemrev.8b00728')
#doi_list.append('https://doi.org/10.1021/acs.jpclett.1c03335 ')
doi_list.append('https://doi.org/10.1021/acs.chemrestox.5b00481')
error_list = Processing(doi_list, 2, 2, 'test481.json')
print(error_list)

del doi_list
del error_list
gc.collect()
