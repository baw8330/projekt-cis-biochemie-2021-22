# -*- coding: utf-8 -*-
"""
Functions that format the computed graph to match the interface to the output-part and saves as a json file

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"

# __copyright__ = ""
# __credits__ = ["", "", "", ""]
# __license__ = ""
# __version__ = ""
# __maintainer__ = ""

import json


def format_nodes(nodes):
    '''
        :param nodes:       list of publications to export to json
        :type nodes:        List[Publication]

        creates a list that contains a dictionary for each node
    '''
    list_of_node_dicts = list()
    for node in nodes:
        new_dict = dict()
        new_dict["doi"] = node.doi_url
        new_dict["name"] = node.title
        new_dict["author"] = node.contributors
        new_dict["year"] = node.publication_date
        new_dict["journal"] = node.journal
        new_dict["abstract"] = node.abstract
        if (node.group == 0):
            new_dict["group"] = "Input"
        elif (node.group > 0):
            new_dict["group"] = "Citedby"
        else:
            new_dict["group"] = "Reference"
        new_dict["depth"] = node.group
        new_dict["citations"] = len(node.citations)
        list_of_node_dicts.append(new_dict)
    return list_of_node_dicts

   
def format_edges(edges):
    '''
        :param edges:       list of links to export to json
        :type edges:        List[String,String]

        function to format links, append to list and return list to output_to_json
    '''
    list_of_edge_dicts = list()
    for edge in edges:
        new_dict_2 = dict()
        new_dict_2["source"] = edge[0]
        new_dict_2["target"] = edge[1]
        list_of_edge_dicts.append(new_dict_2)
    return list_of_edge_dicts


def output_to_json(nodes, edges, search_depth, search_height, json_file='json_text.json', test_var=False):
    '''
        :param nodes:       list of publications to export to json
        :type nodes:        List[Publication]

        :param edges:       list of links to export to json
        :type edges:        List[String,String]

        :param test_var:    variable to differentiate between test and url call
        :type test_var:     boolean

        function to export nodes and links as a dictionary to a given json file
    '''
    dict_of_all = dict()
    list_of_node_dicts = format_nodes(nodes)
    list_of_edge_dicts = format_edges(edges)
    dict_of_all["nodes"] = list_of_node_dicts
    dict_of_all["links"] = list_of_edge_dicts
    dict_of_all["depth_height"] = [search_depth, search_height]

    # output to json. json name depends on test_var and if a non standard filename was given. 
    if (test_var):
        if json_file != 'json_text.json':
            with open(json_file, 'w') as outfile:
                json.dump(dict_of_all, outfile)
        else:
            with open('test_output.json', 'w') as outfile:
                json.dump(dict_of_all, outfile)
    else:
        with open(json_file, 'w') as outfile:
            json.dump(dict_of_all, outfile)
