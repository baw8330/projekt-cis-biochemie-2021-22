# -*- coding: utf-8 -*-
"""
Functions to unittest functions which construct a new graph

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"

# __copyright__ = ""
# __credits__ = ["", "", "", ""]
# __license__ = ""
# __version__ = ""
# __maintainer__ = ""


import unittest
import sys

sys.path.append("../")

from verarbeitung.construct_new_graph.initialize_graph import init_graph_construction, initialize_nodes_list_test, \
    complete_inner_edges_test
from verarbeitung.construct_new_graph.add_citations_rec import get_cit_type_list, create_graph_structure_citations_test
from verarbeitung.construct_new_graph.export_to_json import format_nodes, format_edges
from verarbeitung.get_pub_from_input import input_test_func


class ConstructionTest(unittest.TestCase):
    maxDiff = None

    def testCycle(self):
        nodes, edges, err_list = init_graph_construction(['doiz1'], 1, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doiz1', 'doiz2'])
        self.assertCountEqual(edges, [['doiz1', 'doiz2'], ['doiz2', 'doiz1']])

        nodes, edges, err_list = init_graph_construction(['doiz1'], 2, 2, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doiz1', 'doiz2'])
        self.assertCountEqual(edges, [['doiz2', 'doiz1'], ['doiz1', 'doiz2']])

    def testEmptyDepthHeight(self):
        nodes, edges, err_list = init_graph_construction(['doi1'], 0, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi1'])
        self.assertCountEqual(edges, [])

        nodes, edges, err_list = init_graph_construction(['doi1', 'doi2'], 0, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi1', 'doi2'])
        self.assertCountEqual(edges, [['doi1', 'doi2']])

        nodes, edges, err_list = init_graph_construction(['doi1', 'doi2', 'doi3'], 0, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi1', 'doi2', 'doi3'])
        self.assertCountEqual(edges, [['doi3', 'doi1'], ['doi1', 'doi2']])

    def testInnerEdges(self):
        nodes, edges, err_list = init_graph_construction(['doi_ie1'], 1, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_ie1', 'doi_ie2', 'doi_ie3'])
        self.assertCountEqual(edges, [['doi_ie1', 'doi_ie2'], ['doi_ie3', 'doi_ie1'], ['doi_ie3', 'doi_ie2']])

    def testRightHeight(self):
        nodes, edges, err_list = init_graph_construction(['doi_h01'], 0, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_h01'])
        self.assertCountEqual(edges, [])

        nodes, edges, err_list = init_graph_construction(['doi_h02'], 0, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_h02', 'doi_h1'])
        self.assertCountEqual(edges, [['doi_h1', 'doi_h02']])

        nodes, edges, err_list = init_graph_construction(['doi_h02'], 0, 2, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_h02', 'doi_h1', 'doi_h2'])
        self.assertCountEqual(edges, [['doi_h1', 'doi_h02'], ['doi_h2', 'doi_h1']])

    def testRightDepth(self):
        nodes, edges, err_list = init_graph_construction(['doi_d01'], 1, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_d01'])
        self.assertCountEqual(edges, [])

        nodes, edges, err_list = init_graph_construction(['doi_d02'], 1, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_d02', 'doi_d1'])
        self.assertCountEqual(edges, [['doi_d02', 'doi_d1']])

        nodes, edges, err_list = init_graph_construction(['doi_d02'], 2, 0, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi_d02', 'doi_d1', 'doi_d2'])
        self.assertCountEqual(edges, [['doi_d02', 'doi_d1'], ['doi_d1', 'doi_d2']])

    def test_incorrect_input_dois(self):
        nodes, edges, err_list = init_graph_construction(['doi1ic', 'doi2ic'], 1, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, [])
        self.assertCountEqual(edges, [])
        self.assertCountEqual(err_list, ['doi1ic', 'doi2ic'])

        nodes, edges, err_list = init_graph_construction(['doi1ic', 'doi2ic'], 2, 2, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, [])
        self.assertCountEqual(edges, [])
        self.assertCountEqual(err_list, ['doi1ic', 'doi2ic'])

        nodes, edges, err_list = init_graph_construction(['doi1', 'doi2ic'], 1, 1, True, False)
        doi_nodes = keep_only_dois(nodes)
        self.assertCountEqual(doi_nodes, ['doi1', 'doi2', 'doi3'])
        self.assertCountEqual(edges, [['doi1', 'doi2'], ['doi3', 'doi1']])
        self.assertCountEqual(err_list, ['doi2ic'])

    ## from here: tests for the individual functions ##

    # initialize_graph.py:

    def test_initialize_nodes_list(self):
        references_pub_obj_list, citations_pub_obj_list = initialize_nodes_list_test(['doi_lg_1_i', 'doi_lg_2_i'], 0, 0,
                                                                                     True)
        doi_references = keep_only_dois(references_pub_obj_list)
        doi_citations = keep_only_dois(citations_pub_obj_list)
        self.assertCountEqual(doi_references, [])
        self.assertCountEqual(doi_citations, [])

        references_pub_obj_list, citations_pub_obj_list = initialize_nodes_list_test(['doi_lg_1_i', 'doi_lg_2_i'], 1, 1,
                                                                                     True)
        doi_references = keep_only_dois(references_pub_obj_list)
        doi_citations = keep_only_dois(citations_pub_obj_list)
        self.assertCountEqual(doi_references, ['doi_lg_1_d11', 'doi_lg_1_d12', 'doi_lg_2_d11', 'doi_lg_2_d12'])
        self.assertCountEqual(doi_citations,
                              ['doi_lg_1_h11', 'doi_lg_1_h12', 'doi_cg_i', 'doi_lg_2_h11', 'doi_lg_2_h12'])

    def test_complete_inner_edges(self):
        pub_lg_1_i = input_test_func('doi_lg_1_i')
        pub_lg_1_i.group = 0
        pub_lg_1_h_12 = input_test_func('doi_lg_1_h12')
        pub_lg_1_h_12.group = 1
        pub_lg_1_d_12 = input_test_func('doi_lg_1_d12')
        pub_lg_1_d_12.group = -1
        nodes = [pub_lg_1_i, pub_lg_1_h_12, pub_lg_1_d_12]
        edges = [['doi_lg_1_i', 'doi_lg_1_d12'], ['doi_lg_1_h12', 'doi_lg_1_i']]
        processed_nodes, processed_edges = complete_inner_edges_test(nodes, edges)
        self.assertCountEqual(processed_nodes, [pub_lg_1_i, pub_lg_1_h_12, pub_lg_1_d_12])
        self.assertCountEqual(processed_edges, [['doi_lg_1_i', 'doi_lg_1_d12'], ['doi_lg_1_h12', 'doi_lg_1_i'],
                                                ['doi_lg_1_h12', 'doi_lg_1_d12']])

    # add_citations_rec.py:

    def test_get_type_list(self):
        pub_lg_1_i = input_test_func('doi_lg_1_i')
        pub_lg_1_i.group = 0
        self.assertEqual(get_cit_type_list(pub_lg_1_i, "Hallo"), ValueError)

        pub_lg_1_h_12 = input_test_func('doi_lg_1_h12')
        pub_lg_1_h_12.group = 1
        pub_lg_1_h_12_refs = get_cit_type_list(pub_lg_1_h_12, "Reference")
        pub_lg_1_h_12_cits = get_cit_type_list(pub_lg_1_h_12, "Citation")
        self.assertCountEqual(keep_only_dois(pub_lg_1_h_12_refs), keep_only_dois(pub_lg_1_h_12.references))
        self.assertCountEqual(keep_only_dois(pub_lg_1_h_12_cits), keep_only_dois(pub_lg_1_h_12.citations))

        pub_lg_1_d_12 = input_test_func('doi_lg_1_d12')
        pub_lg_1_d_12.group = -1
        pub_lg_1_d_12_refs = get_cit_type_list(pub_lg_1_d_12, "Reference")
        pub_lg_1_d_12_cits = get_cit_type_list(pub_lg_1_d_12, "Citation")
        self.assertCountEqual(keep_only_dois(pub_lg_1_d_12_refs), keep_only_dois(pub_lg_1_d_12.references))
        self.assertCountEqual(keep_only_dois(pub_lg_1_d_12_cits), keep_only_dois(pub_lg_1_d_12.citations))

    def test_create_graph_structure_citations(self):
        pub_lg_1_i = input_test_func('doi_lg_1_i')
        pub_lg_1_i.group = 0
        pub_lg_1_h_11 = input_test_func('doi_lg_1_h11')
        pub_lg_1_h_11.group = 1
        pub_lg_1_h_12 = input_test_func('doi_lg_1_h12')
        pub_lg_1_h_12.group = 1
        pub_lg_1_d_11 = input_test_func('doi_lg_1_d11')
        pub_lg_1_d_11.group = -1
        pub_lg_1_d_12 = input_test_func('doi_lg_1_d12')
        pub_lg_1_d_12.group = -1

        # checks if citations/references are found and added
        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 2, "Citation", True,
                                                                                     [pub_lg_1_i, pub_lg_1_d_11,
                                                                                      pub_lg_1_d_12],
                                                                                     [['doi_lg_1_i', 'doi_lg_1_d11'],
                                                                                      ['doi_lg_1_i', 'doi_lg_1_d12']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_h_12, pub_lg_1_d_11, pub_lg_1_d_12])
        self.assertCountEqual(return_edges, [['doi_lg_1_i', 'doi_lg_1_d11'], ['doi_lg_1_i', 'doi_lg_1_d12'],
                                             ['doi_lg_1_h11', 'doi_lg_1_i'], ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(cit_list, [pub_lg_1_h_11, pub_lg_1_h_12])

        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 2, "Reference",
                                                                                     True, [pub_lg_1_i, pub_lg_1_h_11,
                                                                                            pub_lg_1_h_12],
                                                                                     [['doi_lg_1_h11', 'doi_lg_1_i'],
                                                                                      ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_h_12, pub_lg_1_d_11, pub_lg_1_d_12])
        self.assertCountEqual(return_edges, [['doi_lg_1_i', 'doi_lg_1_d11'], ['doi_lg_1_i', 'doi_lg_1_d12'],
                                             ['doi_lg_1_h11', 'doi_lg_1_i'], ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(cit_list, [pub_lg_1_d_11, pub_lg_1_d_12])

        # checks if max depth/height is checked before added
        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 1, "Citation", True,
                                                                                     [pub_lg_1_i, pub_lg_1_d_11,
                                                                                      pub_lg_1_d_12],
                                                                                     [['doi_lg_1_i', 'doi_lg_1_d11'],
                                                                                      ['doi_lg_1_i', 'doi_lg_1_d12']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_d_11, pub_lg_1_d_12])
        self.assertCountEqual(return_edges, [['doi_lg_1_i', 'doi_lg_1_d11'], ['doi_lg_1_i', 'doi_lg_1_d12']])
        self.assertCountEqual(cit_list, [])

        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 1, "Reference",
                                                                                     True, [pub_lg_1_i, pub_lg_1_h_11,
                                                                                            pub_lg_1_h_12],
                                                                                     [['doi_lg_1_h11', 'doi_lg_1_i'],
                                                                                      ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_h_12])
        self.assertCountEqual(return_edges, [['doi_lg_1_h11', 'doi_lg_1_i'], ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(cit_list, [])

        # checks if max depth/height is checked before added but citation/reference from max depth/height found and added
        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 1, "Citation", True,
                                                                                     [pub_lg_1_i, pub_lg_1_d_11,
                                                                                      pub_lg_1_d_12, pub_lg_1_h_11],
                                                                                     [['doi_lg_1_i', 'doi_lg_1_d11'],
                                                                                      ['doi_lg_1_i', 'doi_lg_1_d12']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_d_11, pub_lg_1_d_12, pub_lg_1_h_11])
        self.assertCountEqual(return_edges, [['doi_lg_1_i', 'doi_lg_1_d11'], ['doi_lg_1_i', 'doi_lg_1_d12'],
                                             ['doi_lg_1_h11', 'doi_lg_1_i']])
        self.assertCountEqual(cit_list, [])

        return_nodes, return_edges, cit_list = create_graph_structure_citations_test(pub_lg_1_i, 1, 1, "Reference",
                                                                                     True, [pub_lg_1_i, pub_lg_1_h_11,
                                                                                            pub_lg_1_h_12,
                                                                                            pub_lg_1_d_11],
                                                                                     [['doi_lg_1_h11', 'doi_lg_1_i'],
                                                                                      ['doi_lg_1_h12', 'doi_lg_1_i']])
        self.assertCountEqual(return_nodes, [pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_h_12, pub_lg_1_d_11])
        self.assertCountEqual(return_edges, [['doi_lg_1_h11', 'doi_lg_1_i'], ['doi_lg_1_h12', 'doi_lg_1_i'],
                                             ['doi_lg_1_i', 'doi_lg_1_d11']])
        self.assertCountEqual(cit_list, [])

    # export_to_json.py:

    def test_format_nodes(self):
        pub_lg_1_i = input_test_func('doi_lg_1_i')
        pub_lg_1_i.group = 0
        pub_lg_1_h_11 = input_test_func('doi_lg_1_h11')
        pub_lg_1_h_11.group = 1
        pub_lg_1_d_11 = input_test_func('doi_lg_1_d11')
        pub_lg_1_d_11.group = -1

        return_list_of_node_dicts = format_nodes([pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_d_11])
        check_list_of_node_dicts = [
            {"doi": 'doi_lg_1_i', "name": 'title_lg_1_i', "author": ['contributor_lg_1_i'], "year": 'date_lg_1_i',
             "journal": 'journal_lg_1_i', "abstract": None, "group": 'Input', "depth": 0, "citations": 2},
            {"doi": 'doi_lg_1_h11', "name": 'title_lg_1_h11', "author": ['contributor_lg_1_h11'],
             "year": 'date_lg_1_h11', "journal": 'journal_lg_1_h11', "abstract": None, "group": 'Citedby', "depth": 1,
             "citations": 2},
            {"doi": 'doi_lg_1_d11', "name": 'title_lg_1_d11', "author": ['contributor_lg_1_d11'],
             "year": 'date_lg_1_d11', "journal": 'journal_lg_1_d11', "abstract": None, "group": 'Reference',
             "depth": -1, "citations": 1}]

        self.assertCountEqual(return_list_of_node_dicts, check_list_of_node_dicts)

    def test_format_edges(self):
        return_list_of_edges = format_edges(
            [['doi_lg_1_i', 'doi_lg_1_d11'], ['doi_lg_1_i', 'doi_lg_1_d12'], ['doi_lg_1_h11', 'doi_lg_1_i'],
             ['doi_lg_1_h12', 'doi_lg_1_i']])
        check_list_of_edges = [{"source": 'doi_lg_1_i', "target": 'doi_lg_1_d11'},
                               {"source": 'doi_lg_1_i', "target": 'doi_lg_1_d12'},
                               {"source": 'doi_lg_1_h11', "target": 'doi_lg_1_i'},
                               {"source": 'doi_lg_1_h12', "target": 'doi_lg_1_i'}]

        self.assertCountEqual(return_list_of_edges, check_list_of_edges)


def keep_only_dois(nodes):
    '''
          :param nodes:  input list of nodes of type Publication
          :type nodes:   List[Publication]

          gets nodes of type pub and return only their DOI
    '''
    doi_list = []
    for node in nodes:
        doi_list.append(node.doi_url)
    return doi_list


if __name__ == "__main__":
    unittest.main()
