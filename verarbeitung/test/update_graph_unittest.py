# -*- coding: utf-8 -*-
"""
Functions to unittest functions which are updating a known graph

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"

# __copyright__ = ""
# __credits__ = ["", "", "", ""]
# __license__ = ""
# __version__ = ""
# __maintainer__ = ""


import unittest
import sys
from pathlib import Path

sys.path.append("../")

from verarbeitung.construct_new_graph.initialize_graph import init_graph_construction
from verarbeitung.construct_new_graph.export_to_json import output_to_json
from verarbeitung.update_graph.import_from_json import input_from_json
from verarbeitung.update_graph.update_graph import update_graph, get_old_input_dois, get_new_input_dois
from verarbeitung.update_graph.update_depth import reduce_max_height_depth_test, get_old_max_references_citations_test
from verarbeitung.update_graph.update_edges import back_to_valid_edges
from verarbeitung.update_graph.delete_nodes_edges import search_ref_cit_graph_rec_test
from verarbeitung.update_graph.compare_old_and_new_node_lists import compare_old_and_new_node_lists
from verarbeitung.update_graph.connect_new_input import find_furthermost_citations_test, \
    complete_changed_group_nodes_test
from verarbeitung.get_pub_from_input import input_test_func


class UpdatingTest(unittest.TestCase):
    maxDiff = None

    def test_deleted_input_dois(self):
        nodes_old_single, edges_old_single, err_list = init_graph_construction(['doi_lg_1_i'], 2, 2, True)
        nodes_old_both, edges_old_both, err_list = init_graph_construction(['doi_lg_1_i', 'doi_lg_2_i'], 2, 2, True)
        output_to_json(nodes_old_both, edges_old_both, 2, 2, test_var=True)
        nodes_new_single, edges_new_single, err_list = update_graph(['doi_lg_1_i'], 'test_output.json', 2, 2, True)
        self.assertCountEqual(nodes_old_single, nodes_new_single)
        self.assertCountEqual(edges_old_single, edges_new_single)

        nodes_old_single, edges_old_single, err_list = init_graph_construction(['doi_cg_i'], 3, 3, True)
        nodes_old_two, edges_old_two, err_list = init_graph_construction(['doi_lg_1_i', 'doi_cg_i'], 3, 3, True)
        nodes_old_three, edges_old_three, err_list = init_graph_construction(['doi_lg_1_i', 'doi_lg_2_i', 'doi_cg_i'],
                                                                             3, 3, True)

    def test_new_height(self):
        nodes_height_0, edges_height_0, err_list = init_graph_construction(['doi_lg_1_i'], 2, 0, True)
        nodes_height_1, edges_height_1, err_list = init_graph_construction(['doi_lg_1_i'], 2, 1, True)
        nodes_height_2, edges_height_2, err_list = init_graph_construction(['doi_lg_1_i'], 2, 2, True)

        output_to_json(nodes_height_2, edges_height_2, 2, 2, 'new_height.json', True)
        nodes_new_height_1, edges_new_height_1, err_list = update_graph(['doi_lg_1_i'], 'new_height.json', 2, 1, True)
        self.assertCountEqual(nodes_height_1, nodes_new_height_1)
        self.assertCountEqual(edges_height_1, edges_new_height_1)

        nodes_height_2, edges_height_2, err_list = init_graph_construction(['doi_lg_1_i'], 2, 2, True)
        output_to_json(nodes_height_2, edges_height_2, 2, 2, 'new_height.json', True)
        nodes_new_height_0, edges_new_height_0, err_list = update_graph(['doi_lg_1_i'], 'new_height.json', 2, 0, True)
        self.assertCountEqual(nodes_height_0, nodes_new_height_0)
        self.assertCountEqual(edges_height_0, edges_new_height_0)

    def test_ref_to_input(self):
        nodes, edges, err_list = init_graph_construction(['doi_cg_i'], 2, 2, True)
        nodes_2, edges_2, err_list_2 = init_graph_construction(['doi_cg_d11'], 2, 2, True)
        output_to_json(nodes, edges, 2, 2, 'ref_to_input.json')
        new_nodes, new_edges, new_err_list = update_graph(['doi_cg_d11'], 'ref_to_input.json', 2, 2, True)
        self.assertCountEqual(new_nodes, nodes_2)
        self.assertCountEqual(new_edges, edges_2)

        nodes, edges, err_list = init_graph_construction(['doi_cg_i', 'doi_lg_2_i'], 2, 2, True)
        nodes_2, edges_2, err_list_2 = init_graph_construction(['doi_cg_d11', 'doi_lg_2_i'], 2, 2, True)

        output_to_json(nodes, edges, 2, 2, 'ref_to_input.json')
        new_nodes, new_edges, new_err_list = update_graph(['doi_cg_d11', 'doi_lg_2_i'], 'ref_to_input.json', 2, 2, True)
        self.assertCountEqual(new_nodes, nodes_2)
        self.assertCountEqual(new_edges, edges_2)

        output_to_json(nodes_2, edges_2, 2, 2, 'ref_to_input.json')
        new_nodes, new_edges, new_err_list = update_graph(['doi_cg_d11', 'doi_lg_2_i'], 'ref_to_input.json', 2, 2, True)
        self.assertCountEqual(new_nodes, nodes_2)
        self.assertCountEqual(new_edges, edges_2)

        nodes, edges, err_list = init_graph_construction(['doi_cg_i', 'doi_lg_2_i'], 2, 2, True)
        nodes_2, edges_2, err_list_2 = init_graph_construction(['doi_cg_i', 'doi_lg_2_h11', 'doi_lg_1_i'], 3, 3, True)

        output_to_json(nodes_2, edges_2, 2, 2, 'ref_to_input.json')
        new_nodes, new_edges, new_err_list = update_graph(['doi_cg_i', 'doi_lg_2_i'], 'ref_to_input.json', 2, 2, True)
        self.assertCountEqual(new_nodes, nodes)
        self.assertCountEqual(new_edges, edges)

    ## From here: tests for the individual functions ##

    # update_graph.py:

    def test_get_old_input_dois(self):
        pub_lg_1_i = input_test_func('doi_lg_1_i')
        pub_lg_1_i.group = 0
        pub_lg_1_h_11 = input_test_func('doi_lg_1_h11')
        pub_lg_1_h_11.group = 1
        pub_lg_1_d_11 = input_test_func('doi_lg_1_d11')
        pub_lg_1_d_11.group = -1
        old_pubs = [pub_lg_1_i, pub_lg_1_h_11, pub_lg_1_d_11]
        self.assertCountEqual(get_old_input_dois(old_pubs), ['doi_lg_1_i'])

    # hard to test because we only have DOIs as test objects and no urls variant
    def test_get_new_input_dois(self):
        new_dois = ['doi_lg_2_i', 'doi_lg_1_i', 'doi_cg_i']
        self.assertCountEqual(get_new_input_dois(new_dois, True), ['doi_lg_2_i', 'doi_lg_1_i', 'doi_cg_i'])

    # update_depth.py:

    def test_reduce_max_height(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h_11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h_11.group = 1
        pub_lg_2_d_11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d_11.group = -1
        pub_lg_2_h_21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h_21.group = 2
        pub_lg_2_d_21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d_21.group = -2
        pubs = [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_d_11, pub_lg_2_d_21]
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 2, "Height"),
                              [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_d_11, pub_lg_2_d_21])
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 1, "Height"),
                              [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_d_11, pub_lg_2_d_21])
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 0, "Height"),
                              [pub_lg_2_i, pub_lg_2_d_11, pub_lg_2_d_21])

    def test_reduce_max_depth(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h_11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h_11.group = 1
        pub_lg_2_d_11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d_11.group = -1
        pub_lg_2_h_21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h_21.group = 2
        pub_lg_2_d_21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d_21.group = -2
        pubs = [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_d_11, pub_lg_2_d_21]
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 2, "Depth"),
                              [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_d_11, pub_lg_2_d_21])
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 1, "Depth"),
                              [pub_lg_2_i, pub_lg_2_d_11, pub_lg_2_h_11, pub_lg_2_h_21])
        self.assertCountEqual(reduce_max_height_depth_test(pubs, 0, "Depth"),
                              [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21])

    def test_get_old_max_references(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h_11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h_11.group = 1
        pub_lg_2_d_11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d_11.group = -1
        pub_lg_2_h_21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h_21.group = 2
        pub_lg_2_d_21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d_21.group = -2
        pub_lg_2_d_22 = input_test_func('doi_lg_2_d22')
        pub_lg_2_d_22.group = -2
        pubs = [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_d_11, pub_lg_2_d_21, pub_lg_2_d_22]
        self.assertCountEqual(get_old_max_references_citations_test(pubs, 2, "Depth"), [pub_lg_2_d_21, pub_lg_2_d_22])

    def test_get_old_max_citations(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h_11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h_11.group = 1
        pub_lg_2_d_11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d_11.group = -1
        pub_lg_2_h_21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h_21.group = 2
        pub_lg_2_h_22 = input_test_func('doi_lg_2_h22')
        pub_lg_2_h_22.group = 2
        pub_lg_2_d_21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d_21.group = -2
        pubs = [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_h_21, pub_lg_2_h_22, pub_lg_2_d_11, pub_lg_2_d_21]
        self.assertCountEqual(get_old_max_references_citations_test(pubs, 2, "Height"), [pub_lg_2_h_21, pub_lg_2_h_22])

    # import_from_json.py:

    def test_input_from_json(self):
        nodes_old, edges_old, err_list = init_graph_construction(['doi_lg_1_i'], 2, 2, True)
        output_to_json(nodes_old, edges_old, 2, 2, test_var=True)
        nodes_new, edges_new, old_depth, old_height = input_from_json('test_output.json')
        self.assertCountEqual(nodes_old, nodes_new)
        self.assertCountEqual(edges_old, edges_new)

    # update_edges.py:

    def test_back_to_valid_edges(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h_11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h_11.group = 1
        pub_lg_2_d_11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d_11.group = -1
        pubs = [pub_lg_2_i, pub_lg_2_h_11, pub_lg_2_d_11]
        edges = [['doi_lg_2_h11', 'doi_lg_2_i'], ['doi_lg_2_i', 'doi_lg_2_d11'], ['doi_lg_2_h21', 'doi_lg_2_h11'],
                 ['doi_lg_2_i', 'doi_lg_2_d21']]
        back_to_valid_edges(edges, pubs)
        self.assertCountEqual([['doi_lg_2_h11', 'doi_lg_2_i'], ['doi_lg_2_i', 'doi_lg_2_d11']], edges)

    # delete_nodes_edges.py:

    def test_search_ref_graph_rec(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h11.group = 1
        pub_lg_2_h12 = input_test_func('doi_lg_2_h12')
        pub_lg_2_h12.group = 1
        pub_lg_2_d11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d11.group = -1
        pub_lg_2_d12 = input_test_func('doi_lg_2_d12')
        pub_lg_2_d12.group = -1
        pub_lg_2_h21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h21.group = 2
        pub_lg_2_h22 = input_test_func('doi_lg_2_h22')
        pub_lg_2_h22.group = 2
        pub_lg_2_d21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d21.group = -2

        pub_cg_i = input_test_func('doi_cg_i')
        pub_cg_i.group = 0
        pub_cg_h11 = input_test_func('doi_cg_h11')
        pub_cg_h11.group = 1
        pub_cg_d12 = input_test_func('doi_cg_d11')
        pub_cg_d12.group = -1
        pub_cg_d11 = input_test_func('doi_cg_d12')
        pub_cg_d11.group = -1
        pubs = [pub_lg_2_i, pub_lg_2_h11, pub_lg_2_h12, pub_lg_2_d11, pub_lg_2_d12, pub_lg_2_h21, pub_lg_2_h22,
                pub_lg_2_d21, pub_cg_i, pub_cg_d11, pub_cg_d12, pub_cg_h11]
        usable_nodes = search_ref_cit_graph_rec_test(pubs, [pub_cg_i], 2, "Citation")
        self.assertCountEqual(usable_nodes, [pub_cg_h11, pub_lg_2_h11, pub_lg_2_h21, pub_lg_2_h22])

    # compare_old_and_new_node_lists.py:

    def test_compare_old_and_new_nodes(self):
        old_input = ['doi_lg_1_i', 'doi_lg_2_i']
        new_input = ['doi_lg_1_i', 'doi_cg_i']
        common_nodes, inserted_nodes, deleted_nodes = compare_old_and_new_node_lists(old_input, new_input)
        self.assertCountEqual(common_nodes, ['doi_lg_1_i'])
        self.assertCountEqual(inserted_nodes, ['doi_cg_i'])
        self.assertCountEqual(deleted_nodes, ['doi_lg_2_i'])

    # connect_new_input.py:

    def test_find_furthermost_citations(self):
        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h11.group = 1
        pub_lg_2_h12 = input_test_func('doi_lg_2_h12')
        pub_lg_2_h12.group = 1
        pub_lg_2_d11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d11.group = -1
        pub_lg_2_d12 = input_test_func('doi_lg_2_d12')
        pub_lg_2_d12.group = -1
        pub_lg_2_h21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h21.group = 2
        pub_lg_2_h22 = input_test_func('doi_lg_2_h22')
        pub_lg_2_h22.group = 2
        pub_lg_2_d21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d21.group = -2
        pub_lg_2_d22 = input_test_func('doi_lg_2_d22')
        pub_lg_2_d22.group = -2
        pubs = [pub_lg_2_i, pub_lg_2_h11, pub_lg_2_h12, pub_lg_2_d11, pub_lg_2_d12, pub_lg_2_h21, pub_lg_2_h22,
                pub_lg_2_d21, pub_lg_2_d22]

        self.assertCountEqual(find_furthermost_citations_test(pubs, [], pub_lg_2_h11, 2, 2, "Citation"),
                              [pub_lg_2_h21, pub_lg_2_h22])
        self.assertCountEqual(find_furthermost_citations_test(pubs, [], pub_lg_2_h11, 2, 1, "Citation"),
                              [pub_lg_2_h21, pub_lg_2_h22])

        self.assertCountEqual(find_furthermost_citations_test(pubs, [], pub_lg_2_d11, 2, 2, "Reference"),
                              [pub_lg_2_d21, pub_lg_2_i])
        self.assertCountEqual(find_furthermost_citations_test(pubs, [], pub_lg_2_d11, 2, 1, "Reference"),
                              [pub_lg_2_d21, pub_lg_2_i])

    def test_complete_changed_group_nodes(self):
        pub_cg_i = input_test_func('doi_cg_i')
        pub_cg_i.group = 0
        pub_cg_h11 = input_test_func('doi_cg_h11')
        pub_cg_h11.group = 1
        pub_cg_h21 = input_test_func('doi_cg_h21')
        pub_cg_h21.group = 2
        pub_cg_h22 = input_test_func('doi_cg_h22')
        pub_cg_h22.group = 2
        pub_cg_d11 = input_test_func('doi_cg_d11')
        pub_cg_d11.group = -1
        pub_cg_d12 = input_test_func('doi_cg_d12')
        pub_cg_d12.group = -1
        pub_cg_d21 = input_test_func('doi_cg_d21')
        pub_cg_d21.group = -2
        pub_cg_d22 = input_test_func('doi_cg_d22')
        pub_cg_d22.group = -2

        pub_lg_1_h23 = input_test_func('doi_lg_1_h23')
        pub_lg_1_h23.group = 2
        pub_lg_1_d23 = input_test_func('doi_lg_1_d23')
        pub_lg_1_d23.group = -2

        pub_lg_2_i = input_test_func('doi_lg_2_i')
        pub_lg_2_i.group = 0
        pub_lg_2_h11 = input_test_func('doi_lg_2_h11')
        pub_lg_2_h11.group = 1
        pub_lg_2_h21 = input_test_func('doi_lg_2_h21')
        pub_lg_2_h21.group = 2
        pub_lg_2_h22 = input_test_func('doi_lg_2_h22')
        pub_lg_2_h22.group = 2
        pub_lg_2_d11 = input_test_func('doi_lg_2_d11')
        pub_lg_2_d11.group = -1
        pub_lg_2_d12 = input_test_func('doi_lg_2_d12')
        pub_lg_2_d12.group = -1
        pub_lg_2_d21 = input_test_func('doi_lg_2_d21')
        pub_lg_2_d21.group = -2
        pub_lg_2_d22 = input_test_func('doi_lg_2_d22')
        pub_lg_2_d22.group = -2
        pub_lg_2_d23 = input_test_func('doi_lg_2_d23')
        pub_lg_2_d23.group = -2
        pub_lg_2_d24 = input_test_func('doi_lg_2_d24')
        pub_lg_2_d24.group = -2

        moved_1_pub_cg_i = input_test_func('doi_cg_i')
        moved_1_pub_cg_i.group = 1
        moved_1_pub_cg_h11 = input_test_func('doi_cg_h11')
        moved_1_pub_cg_h11.group = 2
        moved_1_pub_cg_h21 = input_test_func('doi_cg_h21')
        moved_1_pub_cg_h21.group = 3
        moved_1_pub_cg_h22 = input_test_func('doi_cg_h22')
        moved_1_pub_cg_h22.group = 3
        moved_1_pub_cg_d11 = input_test_func('doi_cg_d11')
        moved_1_pub_cg_d11.group = 0

        moved_1_pub_cg_d21 = input_test_func('doi_cg_d21')
        moved_1_pub_cg_d21.group = -1

        moved_1_pub_lg_1_h23 = input_test_func('doi_lg_1_h23')
        moved_1_pub_lg_1_h23.group = 2
        moved_1_pub_lg_1_d23 = input_test_func('doi_lg_1_d23')
        moved_1_pub_lg_1_d23.group = -1

        moved_1_pub_lg_2_h11 = input_test_func('doi_lg_2_h11')
        moved_1_pub_lg_2_h11.group = 1

        moved_2_pub_cg_i = input_test_func('doi_cg_i')
        moved_2_pub_cg_i.group = -1
        moved_2_pub_cg_d11 = input_test_func('doi_cg_d11')
        moved_2_pub_cg_d11.group = -2
        moved_2_pub_cg_d12 = input_test_func('doi_cg_d12')
        moved_2_pub_cg_d12.group = -2
        moved_2_pub_cg_d21 = input_test_func('doi_cg_d21')
        moved_2_pub_cg_d21.group = -3
        moved_2_pub_cg_d22 = input_test_func('doi_cg_d22')
        moved_2_pub_cg_d22.group = -3

        moved_2_pub_lg_1_d23 = input_test_func('doi_lg_1_d23')
        moved_2_pub_lg_1_d23.group = -3

        moved_2_pub_lg_2_h21 = input_test_func('doi_lg_2_h21')
        moved_2_pub_lg_2_h21.group = 1
        moved_2_pub_lg_2_h22 = input_test_func('doi_lg_2_h22')
        moved_2_pub_lg_2_h22.group = 1
        moved_2_pub_lg_2_h11 = input_test_func('doi_lg_2_h11')
        moved_2_pub_lg_2_h11.group = 0
        moved_2_pub_lg_2_i = input_test_func('doi_lg_2_i')
        moved_2_pub_lg_2_i.group = -2
        moved_2_pub_lg_2_d11 = input_test_func('doi_lg_2_d11')
        moved_2_pub_lg_2_d11.group = -2
        moved_2_pub_lg_2_d12 = input_test_func('doi_lg_2_d12')
        moved_2_pub_lg_2_d12.group = -2
        moved_2_pub_lg_2_d21 = input_test_func('doi_lg_2_d21')
        moved_2_pub_lg_2_d21.group = -3
        moved_2_pub_lg_2_d22 = input_test_func('doi_lg_2_d22')
        moved_2_pub_lg_2_d22.group = -3
        moved_2_pub_lg_2_d23 = input_test_func('doi_lg_2_d23')
        moved_2_pub_lg_2_d23.group = -3
        moved_2_pub_lg_2_d24 = input_test_func('doi_lg_2_d24')
        moved_2_pub_lg_2_d24.group = -3

        pubs = [pub_cg_i, pub_cg_h11, pub_cg_h21, pub_cg_h22, pub_cg_d11, pub_cg_d12, pub_cg_d21, pub_cg_d22,
                pub_lg_1_h23, pub_lg_1_d23, pub_lg_2_h21, pub_lg_2_h22, pub_lg_2_h11, pub_lg_2_i, pub_lg_2_d11,
                pub_lg_2_d12, pub_lg_2_d21, pub_lg_2_d22, pub_lg_2_d23, pub_lg_2_d24]
        edges = []
        nodes, edges, handled_nodes = complete_changed_group_nodes_test(pubs, edges, 'doi_cg_d11', 2, 2, 2, 2)
        self.assertCountEqual(nodes, [moved_1_pub_cg_d11, moved_1_pub_cg_d21, moved_1_pub_lg_1_d23, moved_1_pub_cg_i,
                                      moved_1_pub_lg_1_h23, moved_1_pub_cg_h11, moved_1_pub_lg_2_h11])
        self.assertCountEqual(edges,
                              [['doi_cg_d11', 'doi_lg_1_d23'], ['doi_cg_d11', 'doi_cg_d21'], ['doi_cg_i', 'doi_cg_d11'],
                               ['doi_lg_1_h23', 'doi_cg_i'], ['doi_cg_h11', 'doi_cg_i'], ['doi_lg_2_h11', 'doi_cg_i']])

        nodes, edges, handled_nodes = complete_changed_group_nodes_test(pubs, edges, 'doi_lg_2_h11', 2, 2, 3, 3)
        self.assertCountEqual(nodes, [moved_2_pub_cg_i, moved_2_pub_cg_d11, moved_2_pub_lg_1_d23, moved_2_pub_cg_d21,
                                      moved_2_pub_cg_d12, moved_2_pub_cg_d22, moved_2_pub_lg_2_h21,
                                      moved_2_pub_lg_2_h22, moved_2_pub_lg_2_h11, moved_2_pub_lg_2_i,
                                      moved_2_pub_lg_2_d11, moved_2_pub_lg_2_d21, moved_2_pub_lg_2_d12,
                                      moved_2_pub_lg_2_d22, moved_2_pub_lg_2_d23, moved_2_pub_lg_2_d24])
        self.assertCountEqual(edges,
                              [['doi_cg_d11', 'doi_lg_1_d23'], ['doi_cg_d11', 'doi_cg_d21'], ['doi_cg_i', 'doi_cg_d11'],
                               ['doi_cg_i', 'doi_cg_d12'], ['doi_cg_d12', 'doi_cg_d22'], ['doi_lg_2_h11', 'doi_cg_i'],
                               ['doi_cg_i', 'doi_lg_2_i'], ['doi_lg_2_h21', 'doi_lg_2_h11'],
                               ['doi_lg_2_h22', 'doi_lg_2_h11'], ['doi_lg_2_h11', 'doi_lg_2_i'],
                               ['doi_lg_2_i', 'doi_lg_2_d11'], ['doi_lg_2_d11', 'doi_lg_2_i'],
                               ['doi_lg_2_d11', 'doi_lg_2_d21'], ['doi_lg_2_i', 'doi_lg_2_d12'],
                               ['doi_lg_2_d12', 'doi_lg_2_d22'], ['doi_lg_2_d12', 'doi_lg_2_d23'],
                               ['doi_lg_2_d12', 'doi_lg_2_d24']])


def keep_only_dois(nodes):
    '''
          :param nodes:  input list of nodes of type Publication
          :type nodes:   List[Publication]

          gets nodes of type pub and return only their DOI
     '''
    doi_list = []
    for node in nodes:
        doi_list.append(node.doi_url)
    return doi_list


if __name__ == "__main__":
    unittest.main()
