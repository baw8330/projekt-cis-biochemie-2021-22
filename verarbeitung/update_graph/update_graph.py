1# -*- coding: utf-8 -*-
"""
Functions to update a graph representing citations between multiple ACS/Nature journals

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"

#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""


import sys

sys.path.append("../../")

from input.publication import Publication
from verarbeitung.construct_new_graph.initialize_graph import complete_inner_edges
from verarbeitung.get_pub_from_input import get_pub
from .compare_old_and_new_node_lists import compare_old_and_new_node_lists
from .delete_nodes_edges import delete_nodes_and_edges
from .connect_new_input import connect_old_and_new_input
from .update_depth import update_depth
from .import_from_json import input_from_json


def get_old_input_dois(old_obj_input_list):
    '''
        :param old_obj_input_list:  list of publications retrieved from old json file
        :type old_obj_input_list:   List[Publication]

        function to return pub DOIs for old publications of group input retrieved from json file
    '''

    # new list to save doi_url for each old publication of group input
    old_input_dois = []
    for pub in old_obj_input_list:
        if (pub.group == 0):
            old_input_dois.append(pub.doi_url)
    return old_input_dois


def get_new_input_dois(new_input, test_var):
    '''
        :param new_input:   input list of DOI from UI
        :type new_input:    list of strings

        :param test_var:    variable to differentiate between test and url call
        :type test_var:     boolean

        function to return pub DOIs for input urls
    '''

    # new list to save doi_url for each new input url
    new_input_dois = []
    for new_node in new_input:

        # retrieves information and adds to new list if successful 
        pub = get_pub(new_node, test_var)
        if (type(pub) != Publication):
            error_doi_list.append(new_node)
            continue

        new_input_dois.append(pub.doi_url)
    return(new_input_dois)


def update_graph(new_doi_input_list, json_file, search_depth, search_height, test_var = False):
    '''
        :param new_doi_input_list:  input list of doi from UI
        :type new_doi_input_list:   List[String]

        :param old_obj_input_list:  list of publications retrieved from old json file
        :type old_obj_input_list:   List[Publication]

        :param old_edges_list:      list of links between publications retrieved from old json file
        :type old_edges_list:       List[List[String,String]]

        :param test_var:            variable to differentiate between test and url call
        :type test_var:             boolean

        function to compare old and new input, start node/edge removal and to return updated sets of nodes and edges
    '''

    # gets information from previous construction call
    old_obj_input_list , old_edges_list, old_search_depth, old_search_height = input_from_json(json_file)

    # one global list to save the process of removing unneeded publications and one to save valid edges
    global processed_list, valid_edges, error_doi_list
    processed_list = old_obj_input_list
    valid_edges = old_edges_list
    error_doi_list = []

    # get DOIs from lists to compare for differences
    old_doi_input_list = get_old_input_dois(old_obj_input_list)
    new_doi_input_list = get_new_input_dois(new_doi_input_list, test_var)

    # retrieve which publications are already known, removed, inserted
    common_nodes, inserted_nodes, deleted_nodes = compare_old_and_new_node_lists(old_doi_input_list, new_doi_input_list)

    processed_list_copy = processed_list.copy()
    valid_edges_copy = valid_edges.copy()

    update_depth(processed_list, valid_edges, search_depth, search_height, old_search_depth, old_search_height, test_var)

    # deletes publications and edges from node_list if publications can no longer be reached
    if (len(deleted_nodes) > 0):
        processed_list, valid_edges = delete_nodes_and_edges(processed_list, common_nodes, valid_edges, old_search_depth, old_search_height)

    # returns new lists for nodes and edges if new input dois exist
    if (len(inserted_nodes) > 0):      
        inserted_pub_nodes, inserted_edges, error_doi_list_new = connect_old_and_new_input(processed_list_copy, valid_edges_copy, inserted_nodes, old_search_depth, old_search_height, search_depth, search_height, test_var)
        for err_node in error_doi_list_new:
            if err_node not in error_doi_list:
                error_doi_list.append(err_node)

        # compares new list to processed list and adds new nodes/replaces known nodes
        for inserted_node in inserted_pub_nodes:
            not_in_nodes = True
            for node in processed_list:
                if inserted_node.doi_url == node.doi_url:
                    processed_list.remove(node)
                    processed_list.append(inserted_node)
                    not_in_nodes = False
                    break
            if not_in_nodes:
                processed_list.append(inserted_node)

        # adds new edges to list valid_edges
        for inserted_edge in inserted_edges:
            if inserted_edge not in valid_edges:
                valid_edges.append(inserted_edge)

    # calls function to find cross references between citation and reference group
    complete_inner_edges(True, processed_list, valid_edges)

    return(processed_list, valid_edges, error_doi_list) 
