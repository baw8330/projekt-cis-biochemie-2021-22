# -*- coding: utf-8 -*-
"""
    compares old and new input list to find common, deleted and inserted input dois.

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"

# __copyright__ = ""
# __credits__ = ["", "", "", ""]
# __license__ = ""
# __version__ = ""
# __maintainer__ = ""


from collections import Counter


def compare_old_and_new_node_lists(old_doi_node_list, new_doi_node_list):
    '''
    :param old_doi_node_list:   list of DOIs from old graph
    :type old_doi_node_list:    List[String]
    
    :param new_doi_node_list:   list of DOIs from new graph
    :type new_doi_node_list:    List[String]
    
    function to calculate, which nodes from the old graph are deleted and which are added
    '''
    dois_from_old_graph = old_doi_node_list  # important: no duplicate DOIs
    dois_from_new_graph = new_doi_node_list
    deleted_nodes = []
    common_nodes = []
    inserted_nodes = []
    all_dois = dois_from_old_graph + dois_from_new_graph

    for doi in all_dois:  # iterates over the merged list of new and old DOIs
        if ((all_dois.count(doi) == 2) & (
                doi not in common_nodes)):  # If the DOI occurs twice the node is in the old and the new graph
            common_nodes.append(doi)  # appends the DOI to common ones, if its not already in it
        elif ((doi in dois_from_old_graph) & (
                doi not in dois_from_new_graph)):  # If the DOI occurs once and it is from old graph it is a deleted node
            deleted_nodes.append(doi)  # appends the DOI to deleted ones
        elif ((doi in dois_from_new_graph) & (
                doi not in dois_from_old_graph)):  # if the DOI occurs ince and it is from new graph it is a inserted node
            inserted_nodes.append(doi)  # appends the DOI to the inserted ones
            
    return (common_nodes, inserted_nodes, deleted_nodes)
