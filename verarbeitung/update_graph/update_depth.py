# -*- coding: utf-8 -*-
"""
Functions to update the citation depth of recursive graph construction

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""

import sys
sys.path.append("../../")

from verarbeitung.construct_new_graph.add_citations_rec import add_citations
from verarbeitung.get_pub_from_input import get_pub
from .update_edges import back_to_valid_edges
from input.publication import Publication


def reduce_max_height_depth_test(pubs, max_dh, dh_var):
    '''
        :param pubs:    list of publication to reduce height/depth in
        :type pubs:     List[Publication]

        :param max_dh:  new maximum depth/height to reduce publications in publication list to
        :type max_dh:   int

        :param dh_var:  defines if depth or height gets to be reduced
        :type dh_var:   String 

        for unit test purposes only
    '''
    global processed_input_list
    processed_input_list = pubs

    if dh_var == "Height":
        reduce_max_height(max_dh)
    else:
        reduce_max_depth(max_dh)
    return processed_input_list


def get_old_max_references_citations_test(pubs, old_dh, dh_var):
    '''
        :param pubs:    list of publication to reduce height/depth in
        :type pubs:     List[Publication]

        :param old_dh:  old depth/height to get max references/citations
        :type old_dh:   int

        :param dh_var:  defines if depth or height gets to be reduced
        :type dh_var:   String 

        for unit test purposes only
    '''
    global processed_input_list
    processed_input_list = pubs

    if dh_var == "Height":
        return(get_old_max_citations(old_dh, True))
    else:
        return(get_old_max_references(old_dh, True))


def reduce_max_height(max_height):
    '''
        :param max_height:        new maximum height to reduce publications in publication list to
        :type max_height:         int

        function to remove all publications which are not in new maximum height threshold
    '''
    input_list_del = processed_input_list.copy()
    for pub in input_list_del:
        if (pub.group > 0):
            if (pub.group > max_height):
                processed_input_list.remove(pub)


def reduce_max_depth(max_depth):
    '''
        :param max_depth:   new maximum depth to reduce publications in publication list to
        :type max_depth:    int

        function to remove all publications which are not in new maximum depth threshold
    '''
    input_list_del = processed_input_list.copy()
    for pub in input_list_del:
        if (pub.group < 0):
            if (abs(pub.group) > max_depth):
                processed_input_list.remove(pub)


def get_old_max_references(old_depth, test_var):
    '''
        :param old_depth:       old maximum depth to search for citations
        :type old_depth:        int

        :param test_var:        variable to differentiate between test and url call
        :type test_var:         boolean

        function to get references for new recursive levels
    '''
    old_max_references = []
    for pub in processed_input_list:
        if (pub.group == -old_depth):
            pub = get_pub(pub.doi_url, test_var)
            if (type(pub) != Publication):
                #print(pub)
                continue
            old_max_references.append(pub)
    return(old_max_references)


def get_old_max_citations(old_height, test_var):
    '''
        :param old_height:      old maximum height to search for citations
        :type old_height:       int

        :param test_var:        variable to differentiate between test and url call
        :type test_var:         boolean

        function to get citations for new recursive levels
    '''
    old_max_citations = []
    for pub in processed_input_list:
        if (pub.group == old_height):
            pub = get_pub(pub.doi_url, test_var)
            if (type(pub) != Publication):
                continue
            old_max_citations.append(pub)
    return(old_max_citations)


def update_depth(obj_input_list, input_edges, new_depth, new_height, old_depth, old_height, test_var):
    '''
        :param obj_input_list:  input list of publications of type Publication from update_graph
        :type obj_input_list:   List[Publication]

        :param input_edges:     list of publications from update_graph
        :type input_edges:      List[Publication]

        :param new_depth:       new maximum depth to search for references
        :type new_depth:        int

        :param new_height:      new maximum height to search for citations
        :type new_height:       int

        :param test_var:        variable to differentiate between test and url call
        :type test_var:         boolean

        function to adjust old publication search depth to update call
    '''

    global processed_input_list, valid_edges
    processed_input_list = obj_input_list
    valid_edges = input_edges

    # removes publications and links from recursion levels which aren't needed anymore or adds new ones
    if (old_depth > new_depth):
        reduce_max_depth(new_depth)
    elif (old_depth < new_depth):
        old_max_references = get_old_max_references(old_depth, test_var)
        add_citations(processed_input_list, valid_edges, old_max_references, old_depth, new_depth, "Reference", test_var)
    
    if (old_height > new_height):
        reduce_max_height(new_height)
    elif (old_height < new_height):
        old_max_citations = get_old_max_citations(old_height, test_var)
        add_citations(processed_input_list, valid_edges, old_max_citations, old_height, new_height, "Citation", test_var)

    back_to_valid_edges(valid_edges, processed_input_list)



    

