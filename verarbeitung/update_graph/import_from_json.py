# -*- coding: utf-8 -*-
"""
    Functions to read old json files to recreate old graph structure

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""


import json
import sys
sys.path.append("../")

from input.publication import Publication, Citation


def create_pubs_from_json(input_dict):
    '''
    :param input_dict:  dictionary read from old graph json file
    :type input_dict:   Dict

    creates list of publication retrieved from old json file
    '''

    # iterates over the list of nodes
    for node in input_dict["nodes"]:
        # creates for the nodes the objects class Publication

        pub = Publication(node["doi"], node["name"], node["author"], node["journal"], node["year"], [])
        pub.group = node["depth"]
        # appends the objects to a list
        list_of_nodes_py.append(pub)


def add_ref_and_cit_to_pubs(input_dict):
    '''
    :param input_dict:  dictionary read from old graph json file
    :type input_dict:   Dict

    adds references and citations to retrieved publication list
    '''

    # iterates over the list of edges
    for edge in input_dict["links"]:
        for source in list_of_nodes_py:
            for target in list_of_nodes_py:

                # when correct dois found, adds then as references/citations to publication list
                if ((source.doi_url == edge["source"]) and (target.doi_url == edge["target"])):
                    new_reference = Citation(target.doi_url, target.title, target.journal, target.contributors, "Reference")
                    source.references.append(new_reference)

                    new_citation = Citation(source.doi_url, source.title, source.journal, source.contributors, "Citation")
                    target.citations.append(new_citation)

        # adds edge to list
        list_of_edges_py.append([edge["source"],edge["target"]])


def input_from_json(json_file):
    '''
    :param json_file:   Json file for the old graph
    :type json_file:    String

    retrieves information from old json file to be reused for new graph construction
    '''

    # creates global sets for nodes and edges
    global list_of_nodes_py, list_of_edges_py
    list_of_nodes_py = []
    list_of_edges_py = []

    # opens the json file and saves content in dictionary
    with open(json_file,'r') as file:
        input_dict = json.load(file)

    # creates nodes of Class Publication from input Json file
    create_pubs_from_json(input_dict)

    # adds references and citations to publications and creates edges 
    add_ref_and_cit_to_pubs(input_dict)
    old_depth_height = input_dict["depth_height"]
    old_depth = old_depth_height[0]
    old_height = old_depth_height[1]

    return(list_of_nodes_py, list_of_edges_py, old_depth, old_height)