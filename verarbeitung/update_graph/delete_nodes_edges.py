# -*- coding: utf-8 -*-
"""
    Functions to remove publications/links from nodes/edges list, if they can no longer be reached

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""

import sys
from pathlib import Path
sys.path.append("../../")

from .update_edges import back_to_valid_edges


def search_ref_cit_graph_rec_test(pubs, new_test_input, old_max_depth, cit_type):
    '''
    :param pub:             pub go get appended to usable_nodes
    :type pub:              Publication

    :param new_test_input:  current recursion depth
    :type new_test_input:   int

    :param old_max_depth:   old max search depth
    :type old_max_depth:    int

    :param cit_type:        variable to differentiate citation and reference call
    :type cit_type:         String
    
    for unit test purposes only
    '''

    global usable_nodes, input_obj_list
    usable_nodes = []
    input_obj_list = pubs

    if cit_type == "Reference":
        for pub in new_test_input:
            search_ref_graph_rec(pub, 1, old_max_depth)
    elif cit_type == "Citation":
        for pub in new_test_input:
            search_cit_graph_rec(pub, 1, old_max_depth)
    return usable_nodes


def search_ref_graph_rec(pub, curr_depth, old_max_depth):
    '''
    :param pub: pub go get appended to usable_nodes
    :type pub:  Publication

    :param curr_depth:     current recursion depth
    :type curr_depth:      int

    :param old_max_depth:  old max search depth
    :type old_max_depth:   int
    
    function that appends nodes of group "reference" to list usable_nodes, if they are reachable from input nodes
    '''
    usable_doi_nodes = []
    for reference in pub.references:
        for ref_pub in input_obj_list:
            if ((reference.doi_url == ref_pub.doi_url) and (ref_pub not in usable_nodes)):
                usable_nodes.append(ref_pub)
                usable_doi_nodes.append(ref_pub.doi_url)

                # to find a cyclus and avoid recursion error
                not_in_citations = True
                for citation in pub.citations:
                    if (reference.doi_url == citation.doi_url and citation.doi_url not in usable_doi_nodes):
                        not_in_citations = False
                        break
                if not_in_citations and curr_depth < old_max_depth:
                    search_ref_graph_rec(ref_pub, curr_depth + 1, old_max_depth)


def search_cit_graph_rec(pub, curr_height, old_max_height):
    '''
    :param pub:             pub go get appended to usable_nodes
    :type pub:              Publication

    :param curr_height:     current recursion height
    :type curr_height:      int

    :param old_max_height:  old max search height
    :type old_max_height:   int
    
    function that appends nodes of group "citation" to list usable_nodes, if they are reachable from input nodes
    '''

    usable_doi_nodes = []
    for citation in pub.citations:
        for cit_pub in input_obj_list:
            if ((citation.doi_url == cit_pub.doi_url)):
                if cit_pub not in usable_nodes:
                    usable_nodes.append(cit_pub)
                    usable_doi_nodes.append(cit_pub.doi_url)

                # to find a cycle and avoid recursion error
                not_in_references = True
                for reference in pub.references:
                    if (citation.doi_url == reference.doi_url and reference.doi_url not in usable_doi_nodes):
                        not_in_references = False
                        break
                if not_in_references and curr_height < old_max_height:
                    search_cit_graph_rec(cit_pub,curr_height + 1, old_max_height)



def delete_nodes_and_edges(input_list, common_nodes, old_edges_list, old_depth, old_height):
    '''
    :param input_list:      list of publications to get reduced
    :type input_list:       List[Publication]

    :param common_nodes:    list of input DOIs which are in old and new input call
    :type common_nodes:     List[String]

    :param old_edges_list:  list of links between publications from old call
    :type old_edges_list:   List[List[String,String]]

    :param old_depth:       old max search depth
    :type old_depth:        int

    :param old_height:      old max search height
    :type old_height:       int
    
    function to start recursive node removal for references and citations and to change edge list to valid state
    '''
    global usable_nodes, input_obj_list
    usable_nodes = []
    input_obj_list = input_list

    # starts for every common input node a tree-search and adds found nodes to usable_nodes
    for common in common_nodes:
        for pub in input_obj_list:
            if (common == pub.doi_url):
                usable_nodes.append(pub)
                search_ref_graph_rec(pub, 1, old_depth)
                search_cit_graph_rec(pub, 1, old_height)

    back_to_valid_edges(old_edges_list, usable_nodes)

    return(usable_nodes, old_edges_list)
