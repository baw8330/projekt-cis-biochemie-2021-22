# -*- coding: utf-8 -*-
"""
main function to call to generate a graph representing citations between multiple ACS/Nature journals

"""

__authors__ = "Donna Löding, Alina Molkentin, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Finished"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""


import sys  
from pathlib import Path
from os import error, path

sys.path.append("../")

from verarbeitung.construct_new_graph.export_to_json import output_to_json
from verarbeitung.construct_new_graph.initialize_graph import init_graph_construction
from verarbeitung.update_graph.update_graph import update_graph


def Processing(url_list, search_depth, search_height, json_file = 'json_text.json'):
    '''
        :param url_list:        list of urls to construct publication graph for
        :type url_list:         List[String]
        
        :param search_depth:    maximum depth to search for references
        :type search_depth:     int

        :param search_height:   maximum height to search for citations
        :type search_height:    int

        :param json_file:       file to export graph to
        :type json_file:        String

        main function to construct new or update known publication graph
    '''

    # updates graph if json file is known in directory otherwise starts new graph construction
    json_file = Path(json_file)
    
    if json_file.is_file():
        nodes, edges, error_doi_list = update_graph(url_list, json_file, search_depth, search_height)
    else:
        nodes, edges, error_doi_list = init_graph_construction(url_list, search_depth, search_height)

    # exports graph to given json file name
    output_to_json(nodes, edges, search_depth, search_height, json_file)

    return error_doi_list
