#!/usr/bin/env python3

"""
Interface for the Input-Package only this should be accessed from outside this Package.
Usage:  Create an InputInterface instance and call 'get_publication(url)' to get 
        an publication instance
"""

from os import walk
import importlib
import pathlib
import re
from input.publication import Publication

class InputInterface:
    """
    Singleton which dynamically imports and manages fetchers
    """

    instance = None
    get_path = None
    fetcher_classes=[]


    def __new__(cls, *args, **kwargs):
        """
        Creates and/or returns the one class-instance. (Singleton-Pattern)
        this method is automaticly called before '__init__'
        """
        # checks if an instance exists and if it doesnt, it creates one
        if cls.instance == None:
            # standard '__new__' 
            cls.instance = super(InputInterface, cls).__new__(cls,*args, **kwargs)
        
        return cls.instance


    def __init__(self):
        """
        Initializes the Singleton and imports all modules in <path>/input/get/
        Fetchers in that directory will be autoimported.
        """

        if self.fetcher_classes ==[]:
            self.import_fetcher_classes()
            if self.fetcher_classes ==[]:
                raise ImportError("No specific Fetchers where found at: '{}'"
                                    .format(self.get_path))
        

    def get_publication(self, url: str) -> Publication:
        """
        The interface-method to get a publication-instance
        (including its citations and references)

        Parameters
        ----------
        :param url: url to a publication
        :type url: str
        :return: Publication instance, else ValueError
        """
        
        # Checks every fetcher if it can use url and if it can tries to fetch it
        for fetcher_class in InputInterface.fetcher_classes:
            if fetcher_class.can_use_url(url):
                return fetcher_class.get_publication(url)
            
        # No fetcher for given url was found
        raise ValueError("'{}' is not supported".format(url))
        
    def get_pub_light(self, url: str) -> Publication:
        """
        The interface-method to get a Publication-instance 
        (only for main article)

        Parameters
        ----------
        :param url: url to a publication
        :type url: str
        :return: publication instance, else ValueError
        """
        
        # Checks every fetcher if it can use url and if it can tries to fetch it
        for fetcher_class in InputInterface.fetcher_classes:
            if fetcher_class.can_use_url(url):
                return fetcher_class.get_pub_light(url)
            
        # No fetcher for given url was found
        raise ValueError("'{}' is not supported".format(url))
    
    def get_supported_fetchers(self):
        # print(self.fetcher_classes[0].__name__) Useless right now, 
        # because all classes are called the same
        return [a.__name__ for a in self.fetcher_classes]

    def import_fetcher_classes(self):
        """
        Searches in '<path>/input/get', if there are [a-z]+.py modules (specific Fetchers)
        and tries to import them.
        Saves found modules in 'fetcher_classes'.
        """

        # Path to 'get'-package
        self.get_path = '{}/get'.format(pathlib.Path(__file__).parent.resolve())
        
        # Searches for modules with given Pattern
        fetcher_file_names=[]
        for file in next(walk(self.get_path), (None, None, []))[2]:
            if re.match(r'[a-z]+.py', file) is not None:
                fetcher_file_names.append(file)

        # Tries to import those modules and saves their class
        for file in fetcher_file_names:
            imported = True
            try:
                fetcher_class = importlib.import_module("input.get.{}".format(file[:-3]))
            except Exception:
                # Raises exception if naming convention not correct
                # file: <[a-z]+>.py (like 'acs.py')
                # class: <filename>Fetcher (like 'AcsFetcher')
                imported = False
                raise ImportError("Module '{}' can not be imported".format(file[:-3]))
            try:
                if imported:
                    # Looks if class-name has the naming convention and then adds it to 'fetcher_classes'
                    self.fetcher_classes.append(fetcher_class\
                        .__getattribute__('{}Fetcher'.format(file[:-3].capitalize())))
            except Exception as error:
                ImportError("Module '{}' does not have a '{}Fetcher'-class"\
                        .format(file[:-3],file[:-3].capitalize()))
