#!/usr/bin/env python

from input.get.acs import AcsFetcher as Acs
from input.publication import Publication, Citation
from input.test.test_input import FetcherTestCase


class AcsTestCase(FetcherTestCase):
    """
    Methods with test_* will be detected by unittest and run.
    """

    def test_acs_url(self):
        """
        Test if AcsFetcher recognizes its 'urls'.
        """
        # Positive Testing
        self.can_use_url_test(Acs, "https://doi.org/10.1021/acs.jcim.1c00203"           , True)
        self.can_use_url_test(Acs, "doi.org/10.1021/acs.jcim.1c00203"                   , True)
        self.can_use_url_test(Acs, "10.1021/acs.jcim.1c00203"                           , True)
        self.can_use_url_test(Acs, " 10.1021/acs.jcim.1c00203"                          , True)
        self.can_use_url_test(Acs, "10.1021/acs.jcim.1c00203 "                          , True)
        self.can_use_url_test(Acs, "\t 10.1021/acs.jcim.1c00203  \t\n"                  , True)
        self.can_use_url_test(Acs, "https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203"  , True)

        # Negative Testing
        self.can_use_url_test(Acs, ""                                                   , False)
        self.can_use_url_test(Acs, "https://doi.org/10.1038/219021a0"                   , False)
        self.can_use_url_test(Acs, "https://www.nature.com/articles/219021a0"           , False)
        self.can_use_url_test(Acs, "https://pubs.acs.org/doi/doi.org/10.1021/acs.jcim.1c00203", False)
        


    def test_acs_publication(self):
        """
        Compares fetched Publication with a static one.
        """
        url = "https://doi.org/10.1021/acs.jcim.1c00203"
        self.get_publication_test(Acs, url, self.expectedPubs[url])

    def test_acs_exceptions(self):
        test_url= "https://doi.org/10.1021/acs.jcim.1c002"
        self.get_publication_exception_test(Acs, test_url)
        
    # Dictionary of Expected Results, with url
    expectedPubs = {
       "https://doi.org/10.1021/acs.jcim.1c00203":
        Publication(
           doi_url = "https://doi.org/10.1021/acs.jcim.1c00203",
           title = "AutoDock Vina 1.2.0: New Docking Methods, Expanded Force Field, and Python Bindings",
           contributors = ["Jerome Eberhardt", "Diogo Santos-Martins", "Andreas F. Tillack", "Stefano Forli"],
           journal="Journal of Chemical Information and Modeling",
           publication_date = "19.07.2021",
           subjects = ["Algorithms","Ligands","Molecules","Receptors","Macrocycles"],
           abstract = "AutoDock Vina is arguably one of the fastest and most widely used open-source programs for molecular docking. However, compared to other programs in the AutoDock Suite, it lacks support for modeling specific features such as macrocycles or explicit water molecules. Here, we describe the implementation of this functionality in AutoDock Vina 1.2.0. Additionally, AutoDock Vina 1.2.0 supports the AutoDock4.2 scoring function, simultaneous docking of multiple ligands, and a batch mode for docking a large number of ligands. Furthermore, we implemented Python bindings to facilitate scripting and the development of docking workflows. This work is an effort toward the unification of the features of the AutoDock4 and AutoDock Vina programs. The source code is available at https://github.com/ccsb-scripps/AutoDock-Vina.",
           references = [
            Citation(doi_url = "https://doi.org/10.1002/jcc.21334"
                , title ="AutoDock Vina: improving the speed and accuracy of docking with a new scoring function, efficient optimization, and multithreading"
                , journal="Journal of Computational Chemistry"
                , contributors=["Trott, O.", "Olson, A. J."]
                , cit_type="Reference")
            , Citation(doi_url = "https://doi.org/10.1038/nprot.2016.051"
                , title ="Computational protein-ligand docking and virtual drug screening with the AutoDock suite"
                , journal="Journal of Natural Products"
                , contributors=["Forli, S.","Huey, R.","Pique, M. E.","Sanner, M. F.","Goodsell, D. S.","Olson, A. J."]
                , cit_type="Reference")
            , Citation(title = "A semiempirical free energy force field with charge-based desolvation"
                , doi_url = "https://doi.org/10.1002/jcc.20634"
	            , journal="Journal of Computational Chemistry"
                , contributors=["Huey, R.","Morris, G. M.","Olson, A. J.","Goodsell, D. S."]
                , cit_type="Reference")
            , Citation(title="Accelerating autodock4 with gpus and gradient-based local search"
                , doi_url="https://doi.org/10.1021/acs.jctc.0c01006"
                , journal="Journal of Chemical Theory and Computation"
                , contributors=["Santos-Martins, D.","Solis-Vasquez, L.","Tillack, A. F.","Sanner, M. F.","Koch, A.","Forli, S."]
                , cit_type="Reference")
            , Citation(title="AutoDockFR: Advances in Protein-Ligand Docking with Explicitly Specified Binding Site Flexibility"
                , doi_url="https://doi.org/10.1371/journal.pcbi.1004586"
                , journal="PLoS Computational Biology"
                , contributors=["Ravindranath, P. A.","Forli, S.","Goodsell, D. S.","Olson, A. J.","Sanner, M. F."]
                , cit_type="Reference")
            , Citation(title="Docking flexible cyclic peptides with AutoDock CrankPep"
                , doi_url="https://doi.org/10.1021/acs.jctc.9b00557"
                , journal="Journal of Chemical Theory and Computation"
                , contributors=["Zhang, Y.","Sanner, M. F."]
                , cit_type="Reference")
            , Citation(title="Fast, accurate, and reliable molecular docking with QuickVina 2"
                , doi_url="https://doi.org/10.1093/bioinformatics/btv082"
                , journal="Bioinformatics"
                , contributors=["Alhossary, A.","Handoko, S. D.","Mu, Y.","Kwoh, C.-K."]
                , cit_type="Reference")
            , Citation(title="Lessons learned in empirical scoring with smina from the CSAR 2011 benchmarking exercise"
                , doi_url="https://doi.org/10.1021/ci300604z"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Koes, D. R.","Baumgartner, M. P.","Camacho, C. J."]
                , cit_type="Reference")
            , Citation(title="Vina-Carb: Improving Glycosidic Angles during Carbohydrate Docking"
                , doi_url="https://doi.org/10.1021/acs.jctc.5b00834"
                , journal="Journal of Chemical Theory and Computation"
                , contributors=["Nivedha, A. K.","Thieker, D. F.","Makeneni, S.","Hu, H.","Woods, R. J."]
                , cit_type="Reference")
            , Citation(title="AutoDock VinaXB: implementation of XBSF, new empirical halogen bond scoring function, into AutoDock Vina"
                , doi_url="https://doi.org/10.1186/s13321-016-0139-1"
                , journal="Journal of Cheminformatics"
                , contributors=["Koebel, M. R.","Schmadeke, G.","Posner, R. G.","Sirimulla, S."]
                , cit_type="Reference")
            , Citation(title="Vinardo: A Scoring Function Based on Autodock Vina Improves Scoring, Docking, and Virtual Screening"
                , doi_url="https://doi.org/10.1371/journal.pone.0155183"
                , journal="PLoS One"
                , contributors=["Quiroga, R.","Villarreal, M. A."]
                , cit_type="Reference")
            , Citation(title="Lennard-Jones potential and dummy atom settings to overcome the AUTODOCK limitation in treating flexible ring systems"
                , doi_url="https://doi.org/10.1021/ci700036j"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Forli, S.","Botta, M."]
                , cit_type="Reference")
            , Citation(title="AutoDock4Zn: an improved AutoDock force field for small-molecule docking to zinc metalloproteins"
                , doi_url="https://doi.org/10.1021/ci500209e"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Santos-Martins, D.","Forli, S.","Ramos, M. J.","Olson, A. J."]
                , cit_type="Reference")
            , Citation(title="A force field with discrete displaceable waters and desolvation entropy for hydrated ligand docking"
                , doi_url="https://doi.org/10.1021/jm2005145"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Forli, S.","Olson, A. J."]
                , cit_type="Reference")
            , Citation(title="Directional phosphorylation and nuclear transport of the splicing factor SRSF1 is regulated by an RNA recognition motif"
                , doi_url="https://doi.org/10.1016/j.jmb.2016.04.009"
                , journal="Journal of Molecular Biology"
                , contributors=["Serrano, P.","Aubol, B. E.","Keshwani, M. M.","Forli, S.","Ma, C.-T.","Dutta, S. K.","Geralt, M.","Wüthrich, K.","Adams, J. A."]
                , cit_type="Reference")
            , Citation(title="Covalent docking using autodock: Two-point attractor and flexible side chain methods"
                , doi_url="https://doi.org/10.1002/pro.2733"
                , journal="Protein Science"
                , contributors=["Bianco, G.","Forli, S.","Goodsell, D. S.","Olson, A. J."]
                , cit_type="Reference")
            , Citation(title="Consensus docking: improving the reliability of docking in a virtual screening context"
                , doi_url="https://doi.org/10.1021/ci300399w"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Houston, D. R.","Walkinshaw, M. D."]
                , cit_type="Reference")
            , Citation(title="DockBench: an integrated informatic platform bridging the gap between the robust validation of docking protocols and virtual screening simulations"
                , doi_url="https://doi.org/10.3390/molecules20069977"
                , journal="Molecules"
                , contributors=["Cuzzolin, A.","Sturlese, M.","Malvacio, I.","Ciancetta, A.","Moro, S."]
                , cit_type="Reference")
            , Citation(title="A new force field for molecular mechanical simulation of nucleic acids and proteins"
                , doi_url="https://doi.org/10.1021/ja00315a051"
                , journal="Journal of the American Chemical Society"
                , contributors=["Weiner, S. J.","Kollman, P. A.","Case, D. A.","Singh, U. C.","Ghio, C.","Alagona, G.","Profeta, S.","Weiner, P."]
                , cit_type="Reference")
            , Citation(title="AutoDock Bias: improving binding mode prediction and virtual screening using known protein-ligand interactions"
                , doi_url="https://doi.org/10.1093/bioinformatics/btz152"
                , journal="Bioinformatics"
                , contributors=["Arcon, J. P.","Modenutti, C. P.","Avendaño, D.","Lopez, E. D.","Defelipe, L. A.","Ambrosio, F. A.","Turjanski, A. G.","Forli, S.","Marti, M. A."]
                , cit_type="Reference")
            , Citation(title="Inhomogeneous Fluid Approach to Solvation Thermodynamics. 1. Theory"
                , doi_url="https://doi.org/10.1021/jp9723574"
                , journal="Journal of Physical Chemistry B"
                , contributors=["Lazaridis, T."]
                , cit_type="Reference")
            , Citation(title="Inhomogeneous fluid approach to solvation thermodynamics. 2. Applications to simple fluids"
                , doi_url="https://doi.org/10.1021/jp972358w"
                , journal="Journal of Physical Chemistry B"
                , contributors=["Lazaridis, T."]
                , cit_type="Reference")
            , Citation(title="Grid inhomogeneous solvation theory: Hydration structure and thermodynamics of the miniature receptor cucurbit[7]uril"
                , doi_url="https://doi.org/10.1063/1.4733951"
                , journal="Journal of Chemical Physics"
                , contributors=["Nguyen, C. N.","Young, T. K.","Gilson, M. K."]
                , cit_type="Reference")
            , Citation(title="AutoDock-GIST: Incorporating Thermodynamics of Active-Site Water into Scoring Function for Accurate Protein-Ligand Docking"
                , doi_url="https://doi.org/10.3390/molecules21111604"
                , journal="Molecules"
                , contributors=["Uehara, S.","Tanaka, S."]
                , cit_type="Reference")
            , Citation(title="ZINC20—A Free Ultralarge-Scale Chemical Database for Ligand Discovery"
                , doi_url="https://doi.org/10.1021/acs.jcim.0c00675"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Irwin, J. J.","Tang, K. G.","Young, J.","Dandarchuluun, C.","Wong, B. R.","Khurelbaatar, M.","Moroz, Y. S.","Mayfield, J.","Sayle, R. A."]
                , cit_type="Reference")
            , Citation(title="Structural biology-inspired discovery of novel KRAS–PDEδ inhibitors"
                , doi_url="https://doi.org/10.1021/acs.jmedchem.7b01243"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Jiang, Y.","Zhuang, C.","Chen, L.","Lu, J.","Dong, G.","Miao, Z.","Zhang, W.","Li, J.","Sheng, C."]
                , cit_type="Reference")
            , Citation(title="D3R grand challenge 2015: evaluation of protein–ligand pose and affinity predictions"
                , doi_url="https://doi.org/10.1007/s10822-016-9946-8"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["Gathiaka, S.","Liu, S.","Chiu, M.","Yang, H.","Stuckey, J. A.","Kang, Y. N.","Delproposto, J.","Kubish, G.","Dunbar, J. B.","Carlson, H. A.","Burley, S. K.","Walters, W. P.","Amaro, R. E.","Feher, V. A.","Gilson, M. K."]
                , cit_type="Reference")
            , Citation(title="D3R grand challenge 4: blind prediction of protein–ligand poses, affinity rankings, and relative binding free energies"
                , doi_url="https://doi.org/10.1007/s10822-020-00289-y"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["Parks, C. D.","Gaieb, Z.","Chiu, M.","Yang, H.","Shao, C.","Walters, W. P.","Jansen, J. M.","McGaughey, G.","Lewis, R. A.","Bembenek, S. D.","Ameriks, M. K.","Mirzadegan, T.","Burley, S. K.","Amaro, R. E.","Gilson, M. K."]
                , cit_type="Reference")
            , Citation(title="D3R Grand Challenge 4: prospective pose prediction of BACE1 ligands with AutoDock-GPU"
                , doi_url="https://doi.org/10.1007/s10822-019-00241-9"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["Santos-Martins, D.","Eberhardt, J.","Bianco, G.","Solis-Vasquez, L.","Ambrosio, F. A.","Koch, A.","Forli, S."]
                , cit_type="Reference")
            , Citation(title="Comparison of affinity ranking using AutoDock-GPU and MM-GBSA scores for BACE-1 inhibitors in the D3R Grand Challenge 4"
                , doi_url="https://doi.org/10.1007/s10822-019-00240-w"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["El Khoury, L.","Santos-Martins, D.","Sasmal, S.","Eberhardt, J.","Bianco, G.","Ambrosio, F. A.","Solis-Vasquez, L.","Koch, A.","Forli, S.","Mobley, D. L."]
                , cit_type="Reference")
            , Citation(title="Macrocycle modeling in ICM: benchmarking and evaluation in D3R Grand Challenge 4"
                , doi_url="https://doi.org/10.1007/s10822-019-00225-9"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["Lam, P. C.-H.","Abagyan, R.","Totrov, M."]
                , cit_type="Reference")
            , Citation(title="Directory of useful decoys, enhanced (DUD-E): better ligands and decoys for better benchmarking"
                , doi_url="https://doi.org/10.1021/jm300687e"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Mysinger, M. M.","Carchia, M.","Irwin, J. J.","Shoichet, B. K."]
                , cit_type="Reference")
            , Citation(title="Evaluation of AutoDock and AutoDock Vina on the CASF-2013 benchmark"
                , doi_url="https://doi.org/10.1021/acs.jcim.8b00312"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Gaillard, T."]
                , cit_type="Reference")
            , Citation(title="Autodock vina adopts more accurate binding poses but autodock4 forms better binding affinity"
                , doi_url="https://doi.org/10.1021/acs.jcim.9b00778"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Nguyen, N. T.","Nguyen, T. H.","Pham, T. N. H.","Huy, N. T.","Bay, M. V.","Pham, M. Q.","Nam, P. C.","Vu, V. V.","Ngo, S. T."]
                , cit_type="Reference")
            , Citation(title="Development and validation of a genetic algorithm for flexible docking"
                , doi_url="https://doi.org/10.1006/jmbi.1996.0897"
                , journal="Journal of Molecular Biology"
                , contributors=["Jones, G.","Willett, P.","Glen, R. C.","Leach, A. R.","Taylor, R."]
                , cit_type="Reference")
            , Citation(title="Glide: a new approach for rapid, accurate docking and scoring. 1. Method and assessment of docking accuracy"
                , doi_url="https://doi.org/10.1021/jm0306430"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Friesner, R. A.","Banks, J. L.","Murphy, R. B.","Halgren, T. A.","Klicic, J. J.","Mainz, D. T.","Repasky, M. P.","Knoll, E. H.","Shelley, M.","Perry, J. K."]
                , cit_type="Reference")
            , Citation(title="Surflex: fully automatic flexible molecular docking using a molecular similarity-based search engine"
                , doi_url="https://doi.org/10.1021/jm020406h"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Jain, A. N."]
                , cit_type="Reference")
            , Citation(title="A fast flexible docking method using an incremental construction algorithm"
                , doi_url="https://doi.org/10.1006/jmbi.1996.0477"
                , journal="Journal of Molecular Biology"
                , contributors=["Rarey, M.","Kramer, B.","Lengauer, T.","Klebe, G."]
                , cit_type="Reference")
            , Citation(title="EDock: blind protein–ligand docking by replica-exchange monte carlo simulation"
                , doi_url="https://doi.org/10.1186/s13321-020-00440-9"
                , journal="Journal of Cheminformatics"
                , contributors=["Zhang, W.","Bell, E. W.","Yin, M.","Zhang, Y."]
                , cit_type="Reference")
            , Citation(title="DOCK 6: Impact of new features and current docking performance"
                , doi_url="https://doi.org/10.1002/jcc.23905"
                , journal="Journal of Computational Chemistry"
                , contributors=["Allen, W. J.","Balius, T. E.","Mukherjee, S.","Brozell, S. R.","Moustakas, D. T.","Lang, P. T.","Case, D. A.","Kuntz, I. D.","Rizzo, R. C."]
                , cit_type="Reference")
            , Citation(title="Improving scoring-docking-screening powers of protein–ligand scoring functions using random forest"
                , doi_url="https://doi.org/10.1002/jcc.24667"
                , journal="Journal of Computational Chemistry"
                , contributors=["Wang, C.","Zhang, Y."]
                , cit_type="Reference")
            , Citation(title="ID-Score: a new empirical scoring function based on a comprehensive set of descriptors related to protein–ligand interactions"
                , doi_url="https://doi.org/10.1021/ci300493w"
                , journal="Journal of Chemical Information and Modeling"
                , contributors=["Li, G.-B.","Yang, L.-L.","Wang, W.-J.","Li, L.-L.","Yang, S.-Y."]
                , cit_type="Reference")
            , Citation(title="Further development and validation of empirical scoring functions for structure-based binding affinity prediction"
                , doi_url="https://doi.org/10.1023/a:1016357811882"
                , journal="Journal of Computer-Aided Molecular Design"
                , contributors=["Wang, R.","Lai, L.","Wang, S."]
                , cit_type="Reference")
            , Citation(title="A knowledge-based energy function for protein- ligand, protein- protein, and protein- DNA complexes"
                , doi_url="https://doi.org/10.1021/jm049314d"
                , journal="Journal of Medicinal Chemistry"
                , contributors=["Zhang, C.","Liu, S.","Zhu, Q.","Zhou, Y."]
                , cit_type="Reference")
            , Citation(title="DLIGAND2: an improved knowledge-based energy function for protein–ligand interactions using the distance-scaled, finite, ideal-gas reference state"
                , doi_url="https://doi.org/10.1186/s13321-019-0373-4"
                , journal="Journal of Cheminformatics"
                , contributors=["Chen, P.","Ke, Y.","Lu, Y.","Du, Y.","Li, J.","Yan, H.","Zhao, H.","Zhou, Y.","Yang, Y."]
                , cit_type="Reference")
            , Citation(title="Comparing AutoDock and Vina in ligand/decoy discrimination for virtual screening"
                , doi_url="https://doi.org/10.3390/app9214538"
                , journal="Applied Science"
                , contributors=["Vieira, T. F.","Sousa, S. F."]
                , cit_type="Reference")
            , Citation(title="Benchmark of four popular virtual screening programs: construction of the active/decoy dataset remains a major determinant of measured performance"
                , doi_url="https://doi.org/10.1186/s13321-016-0167-x"
                , journal="Journal of Cheminformatics"
                , contributors=["Chaput, L.","Martinez-Sanz, J.","Quiniou, E.","Rigolet, P.","Saettel, N.","Mouawad, L."]
                , cit_type="Reference")
            , Citation(title="Array programming with NumPy"
                , doi_url="https://doi.org/10.1038/s41586-020-2649-2"
                , journal="Nature"
                , contributors=["Harris, C. R."]
                , cit_type="Reference")
            , Citation(title="Matplotlib: A 2D graphics environment"
                , doi_url="https://doi.org/10.1109/mcse.2007.55"
                , journal="Computing in Science & Engineering"
                , contributors=["Hunter, J. D."]
                , cit_type="Reference")
           ], citations = [
            Citation(doi_url = "https://doi.org/10.1021/acsomega.1c04320"
            , title ="Novel Anti-Hepatitis B Virus Activity of Euphorbia schimperi and Its Quercetin and Kaempferol Derivatives"
            , journal="ACS Omega"
            , contributors=["Mohammad K. Parvez","Sarfaraz Ahmed","Mohammed S. Al-Dosari","Mazin A. S. Abdelwahid","Ahmed H. Arbab","Adnan J. Al-Rehaily","Mai M. Al-Oqail"],cit_type="Citation"),
           
           ]
       )
    }