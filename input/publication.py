#!/usr/bin/env python3

# this is needed for typing pre python 3.9, this maybe has an large Overhead
from typing import Any, List


class Publication:
    """
    Represents a publication
    """
    def __init__(self, doi_url: str, title: str \
                 , contributors: List[str], journal: str \
                 , publication_date: str, subjects: List[str]\
                 , references: List[Any] = None, citations: List[Any] = None\
                 , abstract: str = None):
        """
        Parameters
        ----------
        :param doi_url: doi_url of the publication
        :type doi_url: str
        :param title: title of the publication
        :type title: str
        :param contributors:list of all contributors
        :type contributors: list[]
        :param journal: the journal of the publication
        :type journal: str
        :param publication_date: date of release
        :type publication_date: str (format dd.mm.yyyy)
        :param subjects: the subject of the Publication
        :type subjects: List[str]
        :param references: the Citation which is been referenced by this publication 
        :type references: List[Any]
        :param citations: the Citation which references this publication
        :type citations: List[Any]
        :param abstract: the abstract of this publication
        :typr abstract: str
        :return: None
        """
        self.doi_url = doi_url
        self.title = title
        self.contributors = contributors
        self.journal = journal
        self.publication_date = publication_date
        self.subjects = subjects
        if references is None:
            self.references = []
        else:
            self.references = references
        if citations is None:
            self.citations = []
        else: 
            self.citations = citations
        self.abstract = abstract
        
        # For the 'Verarbeitungsgruppe'
        self.group = None


    def __str__(self) -> str:
        """
        Default string-converter for this class
        """
        return ("Title:\t\t\t{}\n"
                "Doi-url:\t\t{}\n"
                "Authors:\t\t{}\n"
                "Journal:\t\t{}\n"
                "Published on:\t{}\n"
                "Subjects:\t\t{}\n"
                "Abstract:\t\t{}\n"
                "References:\n{}\n\n"
                "Citations:\n{}")\
                .format(self.title, self.doi_url, ", ".join(self.contributors)
                        , self.journal, self.publication_date
                        , ", ".join(self.subjects), self.abstract
                        , "\n".join(self.get_citation_string(self.references))
                        , "\n".join(self.get_citation_string(self.citations)))


    @staticmethod
    def get_citation_string(citations):
        """
        Helper-method for __str__
        """
        if citations == []:
            return ["None"]
        else:
            citation_string = []
            for citation in citations:
                citation_string.append(citation.__str__())
        return citation_string


    def __eq__(self, other) -> bool:
        """
        Compares the unique doi_url of this publications to a publication, citation or string.
        Overrides the "==" operator.
        """
        if type(other) == Publication or type(other) == Citation:
            return self.doi_url == other.doi_url
        else:
            return self.doi_url == other


class Citation:
    def __init__(self, doi_url: str, title: str \
                , journal: str, contributors: List[str] \
                , cit_type: str = "Citation"):
        """
        Parameters
        ----------
        :param doi_url: doi_url of the citation
        :type doi_url: str
        :param title: title of the citation
        :type title: str
        :param journal: the journal of the citation
        :type journal: str
        :param contributors: list of all contributors
        :type contributors: List[str]
        :param cit_type: Specifies if it's a Reference or Citation
        :type cit_type: str
        :return: None
        """

        self.title = title
        self.doi_url = doi_url
        self.journal = journal
        self.contributors = contributors
        self.cit_type = cit_type


    def __str__(self) -> str:
        """
        Default string-converter for this class
        """
        return ("\n"
                "\t{}-Title:\t\t\t{}\n"
                "\t{}-Doi:\t\t\t{}\n"
                "\t{}-Journal:\t\t{}\n"
                "\t{}-Contributors:\t\t{}")\
                .format(self.cit_type, self.title
                      , self.cit_type, self.doi_url
                      , self.cit_type, self.journal
                      , self.cit_type, ", ".join(self.contributors))


    def __eq__(self, other) -> bool:
        """
        Compares the unique doi_url of this publications to a publication, citation or string
        Overrides the "==" operator.
        """
        if type(other) == Publication or type(other) == Citation:
            return self.doi_url == other.doi_url
        else:
            return self.doi_url == other
