#!/usr/bin/env python3

"""
Child class of JournalFetcher
Usage: None, this is just a template and should be ignored
"""

# import re
from input.get.journal_fetcher import JournalFetcher
from input.publication import Publication


class TemplateFetcher(JournalFetcher):

    """
    This is only a template and therefore has no functionality
    """

    # TODO: Naming-Convention:
    #   Class: '[filename]Fetcher'
    #   file: [journal-/organisation-name]
    #       format: "<[a-z]>+.py" allowed
    #   Not having this convention -> not imported
    #   TODO: List of Compatable Journals
    SUPPORTED_JOURNALS = []

    @staticmethod
    def can_use_url(url: str) -> bool:
        """
        Checks if given url links to a supported journal.
        """

        # TODO: Check the URL for compatability
        #   Maybe like:
        #       url_re = re.match(r'(https?://)?(doi.org/)?(10.(\d{4})/\w+.\S+)', url)
        #       if url_re is not None:
        #           return   url_re[4] in SUPPORTED_JOURNALS
        #       else:
        return False

    @staticmethod
    def get_publication(url: str) -> Publication:
        """
        Creates a Publication-instance.
        """

        # Create soup
        try:
            soup = JournalFetcher.get_soup(url)
        except Exception as errir:
            raise error

        # TODO: Fetch data from the HTML
        #   soup = JournalFetcher.get_soup(url)
        #   Check if soup fetched a Paper
        #   doi -- https://doi.org/10.xxxx/....
        #   title
        #   contributors[]
        #   journal -- if journal in JournalFetcher.abbrev_dict: journal = JournalFetcher.abbrev_dict[journal]
        #   publication_date -- dd.mm.yyyy
        #   subjects[]
        #   abstract
        #   references[]
        #   citations[] 
        # TODO: Create new Publication-instance
        #   return Publication(doi_url = doi_url,title = title, contributors = contributors\
        #              , journal = journal,publication_date = published,subjects = subjects\
        #              ,references = references,citations = citations, abstract = abstract)
        return None

    @staticmethod
    def get_pub_light(url: str) -> Publication:
        """
        Creates a Publication-instance without Citations and References.
        """
        # Create soup
        try:
            soup = JournalFetcher.get_soup(url)
        except Exception as errir:
            raise error


        # TODO: Fetch data from the HTML
        #   soup = JournalFetcher.get_soup(url)
        #   Check if soup fetched a Paper
        #   doi -- https://doi.org/10.xxxx/....
        #   title
        #   contributors[]
        #   journal -- if journal in JournalFetcher.abbrev_dict: journal = JournalFetcher.abbrev_dict[journal]
        #   publication_date -- dd.mm.yyyy
        #   subjects[]
        # TODO: Create new Publication-instance
        #   return Publication(doi_url = doi_url,title = title, contributors = contributors\
        #               , journal = journal,publication_date = published, subjects = subjects\
        #               , references = None, citations = None, abstract = None)
        return None
