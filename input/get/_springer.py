#!/usr/bin/env python3

"""
Child class of JournalFetcher
Usage: Check if Url can be used with 'can_use_url'
       and then fetch publication with 'get_publication'
"""

import re
from input.get.journal_fetcher import JournalFetcher
from input.publication import Citation, Publication


class NatureFetcher(JournalFetcher):

    """
    scrapes publication metadata from a provided url
    """

    #   NOTE: nature does not use journal names in doi links, must match by 10.xxxx identifier instead
    SUPPORTED_JOURNALS = ['1038']

    @staticmethod
    def can_use_url(url: str) -> bool:
        """
        Uses regex to extract journal specific substrings in doi.
        TODO: Support non doi-urls, maybe parse specific journals from s[5 digit] part of nature doi if necessary
        """

        

        matched_url = re.match(r'^(https?://)?(doi.org/)?10.(\d{4})/s(\d{5})-(\d{3})-(\d{5})-(\d{1})', url.strip(". \t\r\n"))

        #checks if match exists
        if matched_url is not None:
            return matched_url[3] in NatureFetcher.SUPPORTED_JOURNALS
        else:
            return False
    
    @staticmethod
    def get_pub_light(url: str) -> Publication:
        pass

    @staticmethod
    def get_publication(url: str) -> Publication:
        """
        takes a url, scrapes article and citation metadata from corresponding website, and returns
        that metadata as a Publication instance.
        """

        #creation of soup
        try:
            soup = JournalFetcher.get_soup(url)
        except Exception as error:
            raise error
        
        # raise error if re recognizes pattern, but url isnt correct:
        #   For other Urls
        if soup.text.strip(" \t\n")=="Missing resource null":
            raise ValueError("'{}' is not a valid doi url".format(url))

        #   For dois
        if soup.title is not None:
            if soup.title.text == "Error: DOI Not Found":
                raise ValueError("'{}' matches pattern for 'Nature', but doesnt link to paper.".format(url))

        #fetching metadata from soup
        doi_url = "https://doi.org/" + soup.head.find(attrs={"name": "DOI"}).get("content")
        title = soup.head.find(attrs={"name": "citation_title"}).get("content")
        journal = soup.head.find(attrs={"name": "citation_journal_title"}).get("content")
        published = soup.head.find(attrs={"name": "prism.publicationDate"}).get("content")
        contributors = []
        subjects = []
        references = []

        for creator in soup.head.findAll(attrs={"name": "dc.creator"}):
            contributors.append(creator.get("content"))

        for subject in soup.head.findAll(attrs={"name": "dc.subject"}):
            subjects.append(subject.get("content"))

        for reference in soup.head.findAll(atts={"name": "citation_reference"}):

            if re.match('citation_journal_title=', reference.get("content")):
                ref_doi = re.search(r'citation_doi=(.+); ', reference.get("content"))[1]
                ref_title = re.search(r'citation_title=(.+); ', reference.get("content"))[1]
                ref_journal = re.search(r'citation_journal_title=(.+); ',reference.get("content"))[1]
                ref_contributors = re.split(r', ', re.search(r'citation_author=(.+); ', reference.get("content")))
            else:
                ref_doi = ""
                ref_title = reference.get("content")
                ref_journal = ""
                ref_contributors = ""
            
            references.append(Citation(doi_url=ref_doi, title=ref_title\
                                    , journal=ref_journal, contributors=ref_contributors\
                                    , cit_type="Referenz" ))


        return Publication(doi_url, title, contributors, journal, published, subjects, references)

        # TODO: Exceptions-handling
        #   raise ValueException("Cant Fetch: '{}'".format(error))
        # return None
