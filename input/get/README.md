# Journal Fetcher
In this Directory there are all the Fetcher, to create an publication-instance.  
These Fetchers should inherit from [**JournalFetcher**](journal_fetcher.py).

## Template
There is a [**template file**](template_.py) with no functionality but can be used to get to see what needs to be added.  

## Naming Convention
The filename of the fetcher should be <[a-z]+>.py and the classname should be \<Filename\>Fetcher.  
Example:  
filename: [acs.py](acs.py)  
classname: [AcsFetcher](acs.py)

## Format
Fetchers should keep this format:
- doi_url : *https://doi.org/10.xxxx/...*
- title   : *title*
- contributors: *[contrib_1, contrib_2, ...]*
- journal = *full name*
- publication_date = *dd.mm.yyyy*
- subjects = *[subject_1, subject_2, ...]*
- references = *[reference_1, reference_2, ...]*
- citations = *[citations_1, citation_2, ...]*
- abstract = *abstract without '\n'*


## Tests
Tests for these Modules are in **input/test/**.  
These tests should inherit from [**FetcherTestCase**](../test/test_input.py) and should have positive and negative testcases. Look in [**test_acs.py**](../test/test_acs.py) for reference.
There are also tests which compares the string representation of the publication with a predefined one.[*](../test/test_txt/acs.txt)