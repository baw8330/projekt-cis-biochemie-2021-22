#!/usr/bin/env python3

"""
Parent class for specific Journal
"""

from abc import ABCMeta, abstractmethod
from bs4 import BeautifulSoup
import requests
from input.publication import Publication

class JournalFetcher(metaclass=ABCMeta):
    """
    This is a abstract-class for fetcher modules.
    It defines common functions, common dictionaries and functions to be implemented
    """
    
    @staticmethod
    def get_soup(url: str) -> BeautifulSoup:
        """
        Retrieves webside-html and returns a BeautifulSoup-instance

        Parameters:
        -----------
        :type url: str
        :param url: doi-url to a publication
        :return: BeatifulSoup-instance
        """
        try:
            req = requests.get(url)
        except  requests.exceptions.HTTPError as err:
            raise SystemExit(err)

        return BeautifulSoup(req.content, 'html.parser')


    @staticmethod
    @abstractmethod
    def can_use_url(url: str) -> bool:
        """
        Abstract-function to be implemented in subclass.
        Checks if given url links to a supported journal
        """
        raise AttributeError("JournalFetcher for '{}' hasnt implemented 'can_use_url(url)'".format(url))


    @staticmethod
    @abstractmethod
    def get_publication(url: str) -> Publication:
        """
        Abstract-function to be implemented in subclass.
        Creates a Publication-instance.
        """
        raise AttributeError("JournalFetcher for '{}' hasnt implemented 'get_publication(url)'".format(url))

    @staticmethod
    @abstractmethod
    def get_pub_light(url: str) -> Publication:
        """
        Abstract-function to be implemented in subclass.
        Creates a Publication-instance without Reference and Citation
        """
        raise AttributeError("JournalFetcher for '{}' hasnt implemented 'get_pub_light(url)'".format(url))

    # Dictionary to get from month to number
    mont_to_num= {
        "january": "01",
        "february": "02",
        "march": "03",
        "april": "04",
        "may": "05",
        "june": "06",
        "july": "07",
        "august": "08",
        "september": "09",
        "october": "10",
        "november": "11",
        "december": "12"
    }

    # A Dictionary, which connects abbreviation to whole journal name
    abbrev_dict = {
         "Nat. Protoc.":"Journal of Natural Products"
        ,"Nat. Chem.":"Nature Chemistry"
        ,"Nat. Med.":"Nature Medicine"
        ,"Nat. Commun.":"Nature Communications"
        ,"Nat. Cell Biol.":"Nature Cell Biology"
        ,"Nat. Methods":"Nature Methods"
        ,"Nat. Chem. Biol.":"Nature Chemical Biology"
        ,"J. Am. Chem. Soc.":"Journal of the American Chemical Society"
        ,"J. Chem. Phys.":"Journal of Chemical Physics"
        ,"J. Phys. Chem. B":"Journal of Physical Chemistry B"
        ,"J. Chem. Theory Comput.":"Journal of Chemical Theory and Computation"
        ,"J. Mol. Biol.":"Journal of Molecular Biology"
        ,"J. Comput. Chem.":"Journal of Computational Chemistry"
        ,"J. Cheminf.":"Journal of Cheminformatics"
        ,"J. Med. Chem.":"Journal of Medicinal Chemistry"
        ,"J. Comput.-Aided Mol. Des.":"Journal of Computer-Aided Molecular Design"
        ,"J. Chem. Inf. Model.":"Journal of Chemical Information and Modeling"
        ,"J. Cell Biolog.":"Journal of Cell Biology"
        ,"J. Cell Sci.":"Journal of Cell Science"
        ,"J. Aerosol Sci. Technol.":"Aerosol Science and Technology"
        ,"Mol. Cell":"Molecular Cell"
        ,"Mol. Cell Biol.":"Molecular and Cellular Biology"
        ,"Mol. Biol. Cell":"Molecular Biology of the Cell"
        ,"Exp. Cell Res.":"Experimental Cell Research"
        ,"PLoS Comput. Biol.":"PLoS Computational Biology"
        ,"PLoS One":"PLoS One"
        ,"Protein Sci.":"Protein Science"
        ,"Appl. Sci.":"Applied Science"
        ,"Comput. Sci. Eng.":"Computing in Science & Engineering"
        ,"Beilstein J. Org. Chem.":"Beilstein Journal of Organic Chemistry"
        ,"Biol. Chem.":"Biological Chemistry"
        ,"Isr. J. Chem.":"Israel Journal of Chemistry"
        ,"Proc. Natl. Acad. Sci. U. S. A.":"Proceedings of the National Academy of Sciences of the United States of America"
        ,"Carbohydr. Res.":"Carbohydrate Research"
        ,"Nucleic Acids Res.":"Nucleic Acids Research"
        ,"Build. Environ.":"Building and Environment"
        ,"Sci. Rep.":"Scientific Reports"
        }
