# Projekt CiS-Projekt 2021/22
Input-Package um Publikationsinformationen mit von einer DOI zu holen.

## Usage/Examples

```python
from input.interface import InputInterface as Input
from input.publication import Publication

def main(url):
    inter = Input()
    try:
        pub = inter.get_publication(url)
    except Exception as error:
        raise error

    print(pub)
    pub.title = "Cool new Title"
    print(pub)

if __name__ == "__main__":
    main("https://doi.org/10.1021/acs.chemrev.8b00728")
```

Die zu erwartende Ergebnisse beim rufen der Funktion:
| Input-DOI | Ergebniss |
|-----------|-----------|
| Unterstützt & Korrekt| Eine Publikationsinstanz |
| Unterstützt & Falsch| ValueError|
| nicht Unterstützt | ValueError|

Unterstützte DOIs sind DOIs,welche das DOI-Mustern der unterstützten Journals besitzen. 

### Unterstützte Journals:

- ACS-Journale
- (Springer-Journale)
- ~~Elsevier~~

## Tests

``` c
python -m unittest input/test/<file.py> -v
# for all tests in directory
python -m unittest discover input/test -v
```
## Authors
- Florian Jochens
- Sam Ockenden
- Julius Schenk
