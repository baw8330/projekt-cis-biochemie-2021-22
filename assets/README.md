# Projekt CiS-Projekt 2021/22

Zitierungsnetzwerk erstellt mit **D3.js**

## Verwendung des Porgramms
### Eingabe
Json-Datei **json\_text.json** im Verzeichnis mit folgendem Format
```json
{
    "nodes": [
        {
            "name": <title: string>,
            "author": [<author1: string>, <author2: string>, ...],
            "year": <date: string>,
            "journal": <journal: string>,
            "doi": <doi: string>,
            "group": <"Input"/"Citedby"/"Reference">,
            "citations": <citation: int>
        }, ...
    ],
    "links": [
        {
            "source": <doi: string>,
            "target": <doi: string>
        }, ...
    ]
}
```

### Anzeigen des Zitierungsnetzwerks
Starten eines Python Web Servers:
```sh
   cd <path to file> &&python3 -m http.server <port>
```
Zugriff auf den Server:
[http://0.0.0.0/:\<port\>](http://0.0.0.0/:<port>)

## Dateien im Verzeichnis
- **index.html**: Webseite
- **cn_default.js**: JavaScript-Code für den Graphen in Standardansicht
- **cn_timeline.js**: JavaScript-Code für den Graphen in Zeitstrahlansicht


## Authoren
- Katja Ehlers
- Merle Stahl