# Projekt CiS-Biochemie 2021-22 

# Requirements:

- Alle requirements sind in der 'requirements' Datei im Hauptverzeichnis zu
  finden.
- Installation: pip3 install -r requirements

# Starten des Programms:

Um das Programm nutzen zu können muss zuerst 'citation\_parser\_ui.py', ausgeführt werden und der entstandene Lik in einen Browser eingefügt werden. Danach öffnet sich die Benutzeroberfläche im Browser.


# Übersicht der Benutzeroberfläche:

- Show Info: Durch wiederholtes klicken kann das Fenster ein und aus geblendet werden.

- Input: Die Eingabe erfolgt in Form eines DOI ("Digital Object Identifier") oder Hyperlink

- Drag and drop or click to select a file to upload: Mehrere DOI oder Hyperlinks in einem .txt-Dokument (genau ein Link pro Zeile).

- Reference Depth: die Tiefe der Artikel welche von der Eingabe zitiert werden.

- Cited-by Depth: die Tiefe derjenigen welche de Eingegebenen Artikel Zitieren. 

- Clear All: alle Eingaben werden gelöscht

- Clear Selected: alle markierten Eingaben werden gelöscht

- Generate Graph: generiert den zugehörigen Graphen 

- Update Automatically: automatische Aktualisierung des Graphen bei jeder neuen Eingabe

- Smart Input: direkte Überprüfung der Eingabe auf Richtigkeit zudem wird nicht mehr der DOI oder Hyperlink angezeigt sondern: 
  Der Autor, Das Journal, Das Veröffentlichungsdatum. (muss vor Hinzufügen aktiviert worden sein)

## Autoren
- Isabelle Siebels
- Sebastian David
- Florian Jochens
- Julius Schenk
- Samuel Ockenden
- Alina Molkentin
- Donna Löding
- Malte Schokolowski
- Judith Große
- Katja Ehlers
- Merle Stahl
